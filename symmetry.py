from __future__ import annotations

import numpy as np

from ase import Atoms

import gpaw
from gpaw.rotation import rotation as gpaw_rotation

# This file uses following index notation
# c   Axis index (x,y,z)
# o   create_symmetry operation index
# g   conjugate class index
# i   irrep index

# all_symmetries = ['C1', 'C2', 'C2v', 'C3', 'C6', 'Oh']
all_symmetries = ['C1', 'C2', 'C2v', 'C3', 'Oh']


def find_vector_permutation(R_ac, Rp_ac, allow_fail: bool = False, **kwargs):
    a = np.argmin(np.sum((R_ac[:, None, :] - Rp_ac[None, :, :])**2, axis=2), axis=1)

    if not allow_fail:
        assert np.allclose(Rp_ac[a], R_ac, **kwargs), str(Rp_ac[a]) + ', ' + str(R_ac)
        assert np.allclose(np.diff(np.sort(a)), 1), str(a) + ' not a permutation'
    return a


def get_symmetry_permutations_by_Rac(ops_occ: list[np.ndarray],
                                     R_ac: np.ndarray) -> np.ndarray:
    """ Get symmetry permutations

    Parameters
    ----------
    R_ac
        Distances between atoms

    Returns
    -------
    Matrix Aperm_ao constructed such that operation o maps atom a -> atom Aperm_ao[a, o]
    """
    Aperm_ao = np.zeros((R_ac.shape[0], len(ops_occ)), dtype=int)
    for o, op_cc in enumerate(ops_occ):
        Rp_ac = np.dot(R_ac, op_cc)  # Transform atoms according to op o
        Aperm_ao[:, o] = find_vector_permutation(R_ac, Rp_ac)
    return Aperm_ao


def create_symmetry(name: Atoms | str,
                    verbose: bool = True) -> PointGroupBaseClass:
    if isinstance(name, Atoms):
        return AtomsRepresentation.from_atoms(name).point_group

    if name == 'Oh':
        return OhSymmetry(verbose=verbose)
    if name == 'C2':
        return C2Symmetry(verbose=verbose)
    if name == 'C1':
        return C1Symmetry(verbose=verbose)
    if name == 'C2v':
        return C2vSymmetry(verbose=verbose)
    if name == 'C3':
        return C3Symmetry(verbose=verbose)
    if name == 'C6':
        return C6Symmetry(verbose=verbose)

    raise ValueError("Unknown symmetry %s" % name)


class AtomsRepresentation:

    def __init__(self,
                 atoms: Atoms,
                 point_group: PointGroupBaseClass,
                 explicit_check: bool = True):
        atoms.translate(-atoms.get_center_of_mass())
        self._atoms = atoms
        self._point_group = point_group

        if explicit_check:
            assert self.check_symmetry(atoms, point_group)

    @classmethod
    def from_atoms(cls,
                   atoms: Atoms,
                   verbose: bool = False) -> AtomsRepresentation:
        for name in reversed(all_symmetries):
            point_group = create_symmetry(name, verbose=verbose)
            if cls.check_symmetry(atoms, point_group):
                if verbose:
                    print(f"Detected symmetry: {point_group.name}")
                return cls(atoms, point_group)
        raise RuntimeError('Should detect some symmetry!!')

    @classmethod
    def check_symmetry(cls,
                       atoms: Atoms,
                       point_group: PointGroupBaseClass,
                       tol: float = 1e-6) -> bool:
        sym = 0
        ops_occ = point_group.ops_occ
        R_ac = atoms.get_positions() - atoms.get_center_of_mass()
        for o, op_cc in enumerate(ops_occ):
            Rp_ac = np.dot(R_ac, op_cc)  # Transform atoms according to op o
            P_a = find_vector_permutation(R_ac, Rp_ac, allow_fail=True)
            if np.linalg.norm(Rp_ac[P_a, :] - R_ac[:, :]) < tol:
                sym += 1
        return sym == len(ops_occ)

    @property
    def atoms(self) -> Atoms:
        return self._atoms

    @property
    def point_group(self) -> PointGroupBaseClass:
        return self._point_group


class Shell:  # TODO rename to Orbit

    def __init__(self,
                 atoms_rep: AtomsRepresentation,
                 a_b: list[int]):
        """ Shell

        In this class, the index b refers to local index of atom in shell
        and a to the global index

        Paramaters
        ----------
        atoms_rep
            Atoms representation
        a_b
            list of global atom indices belonging to this shell
        """
        shell_atoms = atoms_rep.atoms[a_b]

        species_b = shell_atoms.get_chemical_symbols()
        self._species = species_b[0]
        assert all([species == self.species for species in species_b])

        self._atoms_rep = atoms_rep
        self._a_b = a_b
        self._R_bc = shell_atoms.get_positions()
        self._P_bo = self.get_symmetry_permutations()

    def __repr__(self):
        return f'Shell {self.a_b} {self.R_bc}'

    @property
    def species(self) -> str:
        return self._species

    @property
    def point_group(self) -> PointGroupBaseClass:
        return self._atoms_rep.point_group

    @property
    def atoms_rep(self) -> AtomsRepresentation:
        return self._atoms_rep

    @property
    def a_b(self) -> list[int]:
        return self._a_b

    @property
    def R_bc(self) -> np.ndarray:
        return self._R_bc

    @property
    def P_bo(self) -> np.ndarray:
        return self._P_bo

    def get_symmetry_permutations(self) -> np.ndarray:
        return get_symmetry_permutations_by_Rac(self.point_group.ops_occ, self.R_bc)


class LCAOShell(Shell):

    def __init__(self, species, atoms_rep, a_b, R_bc, P_bo, setup):
        self._species = species
        self._atoms_rep = atoms_rep
        self._a_b = a_b
        self._R_bc = R_bc
        self._P_bo = P_bo

        l_j = [phit.get_angular_momentum_number() for phit in setup.phit_j]
        self._l_j = l_j

    @property
    def l_j(self) -> list[int]:
        return self._l_j

    @property
    def point_group(self) -> PointGroupBaseClass:
        return self.atoms_rep.point_group

    @classmethod
    def from_shell(cls, shell, setup: 'gpaw.setup.Setup') -> LCAOShell:
        args_copy = (shell._species, shell._atoms_rep, shell._a_b, shell._R_bc, shell._P_bo)
        shell = cls(*args_copy, setup)

        return shell

    def symmetry_operator_general(self,
                                  l_j: list[int],
                                  permutations_bo: np.ndarray,
                                  o: int
                                  ) -> tuple[np.ndarray, np.ndarray]:
        Nm = np.sum(2 * l_j + 1)  # Number of basis functions of the atoms
        I_bb = np.eye(len(permutations_bo))
        P_bb = I_bb[permutations_bo[:, o], :]  # Create a permutation matrix
        op_mm = np.zeros((Nm, Nm))  # Create rotation matrix
        mstart = 0
        op_cc = self.point_group.ops_occ[o].T
        for l in l_j:
            mend = mstart + 2 * l + 1
            op_mm[mstart:mend, mstart:mend] = gpaw_rotation(l, op_cc)
            mstart = mend
        P_XX = np.kron(P_bb, op_mm).T
        return P_XX

    def symmetry_projectors_general(self,
                                    l_j: list[int],
                                    permutations_bo: np.ndarray
                                    ) -> tuple[np.ndarray, np.ndarray]:
        """ Calculate symmtery projectors

        Parameters
        ----------
        l_j
            list of orbital momentum numbers of basis functions

        permutations_bo
            list of permutations of atoms, as returned by get_symmetry_permutations

            b is a local atom index. All atoms must have same basis functions

        Returns
        -------
        Tuple (U_iXx, proj_iXX)

        U_iXx is
        """
        proj_iXX = []

        # Example: dzp, ssppdd, l_j=[0,0,1,1,2,2]
        l_j = np.array(l_j)

        # In addition, that permutations_bo tells us that atom 3 goes to atom 2, when operatio o is performed
        # We need to evaluate permutations_mmo (which is readily from self.point_group.ops_occ)
        character_ig = self.point_group.character_ig
        I_aa = np.eye(len(permutations_bo))
        U_iXx = []

        Nm = np.sum(2 * l_j + 1)  # Number of basis functions of the atoms
        # X is here combined index of atom-basis function

        # Loop over irreps
        for i in range(self.point_group.Ni()):
            proj_XX = 0
            for g, ops_o in enumerate(self.point_group.ops_g):
                chi = character_ig[i, g]
                for o in ops_o:
                    P_bb = I_aa[permutations_bo[:, o], :]  # Create a permutation matrix

                    # Create rotation matrix
                    op_mm = np.zeros((Nm, Nm))
                    mstart = 0
                    op_cc = self.point_group.ops_occ[o].T
                    for l in l_j:
                        mend = mstart + 2 * l + 1
                        op_mm[mstart:mend, mstart:mend] = gpaw_rotation(l, op_cc)
                        mstart = mend

                    # Create blocked rotation matrix with combined atom-basis function index
                    P_XX = np.kron(P_bb, op_mm)
                    proj_XX = proj_XX + chi * P_XX.T
            proj_XX *= self.point_group.character_ig[i, -1] * 1.0 / self.point_group.order

            # Find the range of matrix
            eps_n, psi_Xn = np.linalg.eig(proj_XX)
            # for n in eps_n:
            #     print("\t%.5f+%.5fj" % (n.real, n.imag), end='\n')
            # print()
            idx, = np.where(abs(eps_n) > 1e-4)
            q, r = np.linalg.qr(psi_Xn[:, idx])
            assert q.size == 0 or np.max(np.abs(q.imag)) < 1e-10
            q = q.real
            # q = psi_Xn[:, idx]
            U_iXx.append(q)
            proj_iXX.append(proj_XX)

        return U_iXx, proj_iXX


class Shells:
    def __init__(self, shells: list[Shell]):
        self._shells = shells

    def __repr__(self):
        return 'Shells object with shells:\n' + '\n'.join(['  ' + str(shell) for shell in self.shells])

    def __iter__(self):
        for shell in self.shells:
            yield shell

    @property
    def shells(self) -> list[Shells]:
        return self._shells

    @classmethod
    def from_atoms_representations(cls,
                                   atoms_rep: AtomsRepresentation,
                                   verbose: bool = False) -> Shells:
        # def get_atomic_shells(self, atoms: Atoms) -> list[list[int]]:
        """ Create Shells object from atoms_rep

        The Shells object contains several Shell objects, and each Shell
        contains atoms of same species that are equivalent under all symmetry operations

        Returns
        -------
        Shells object
        """
        ops_occ = atoms_rep.point_group.ops_occ
        atoms = atoms_rep.atoms

        symbols_a = atoms.get_chemical_symbols()
        positions_av = atoms.get_positions() - atoms.get_center_of_mass()
        all_species = np.unique(symbols_a)

        shells = []
        disttol = 1e-1
        # Loop over species
        for species in all_species:
            # All atoms of this species that are yet to be classified
            # x is some local atom index that changes with every iteration of the loop
            species_atomindex_x = np.array([a for a, sym in enumerate(symbols_a) if sym == species])

            while True:
                # Loop until we have classified all atoms
                if len(species_atomindex_x) == 0:
                    break

                # Pick one atom
                a = species_atomindex_x[0]
                pos_v = positions_av[a]

                # Positions of all other atoms yet to be classified
                species_positions_xv = positions_av[species_atomindex_x]

                # Operate on the chosen atom by all possible operations
                rotated_pos_ov = np.array([op_cc @ pos_v for op_cc in ops_occ])

                # Identify which atom each of the rotated positions corresponds to
                distance_xvo = species_positions_xv[:, np.newaxis, :] - rotated_pos_ov[np.newaxis, :, :]
                distance_xo = np.linalg.norm(distance_xvo, axis=2)
                nonzero_x, nonzero_o = np.nonzero(distance_xo < disttol)
                unonzero_x = np.unique(nonzero_x)

                shell_atomindex_b = species_atomindex_x[unonzero_x]

                if verbose:
                    print(f'Shell contains {shell_atomindex_b}')
                    # for (ix, io) in zip(nonzero_x, nonzero_o):
                    #     print(f'  Atom {a} equivalent to {species_atomindex_x[ix]} under symmetry operation {io}')
                shell = Shell(atoms_rep, shell_atomindex_b)
                shells.append(shell)

                species_atomindex_x = np.delete(species_atomindex_x, unonzero_x)

        print(f'Found {len(shells)} shells')
        return Shells(shells)

    def get_symmetry_permutations_by_shell(self) -> list[list[list[np.ndarray]]]:
        """ Get symmetry permutations by shell

        Parameters
        ----------
        atoms
            Atoms

        Returns
        -------
        Matrix Aperm_sbo constructed such that for each shell s operation o maps
        atom b -> atom Aperm_sbo[s, b, o]
        """
        Aperm_sbo = [shell.P_bo for shell in self.shells]
        return Aperm_sbo


class LCAOShells(Shells):
    def __init__(self,
                 shells: Shells,
                 setups: gpaw.setup.Setups):
        setups_sb = [[setups[a] for a in shell.a_b] for shell in shells]
        assert all([all([setup_b[0] == setup for setup in setup_b]) for setup_b in setups_sb])
        self._setups_s = [setups_b[0] for setups_b in setups_sb]
        self._M_a = setups.M_a + [setups.nao]
        super().__init__([LCAOShell.from_shell(shell, setup) for shell, setup in zip(shells, self.setups_s)])

        self.P_sbo = self.get_symmetry_permutations_by_shell()
        self.U_sixy, self.P_sixx = self.symmetry_projectors_by_shell(self.l_sj, self.P_sbo)

    @property
    def l_sj(self) -> list[list[int]]:
        return [shell.l_j for shell in self.shells]

    @property
    def shells(self) -> list[LCAOShells]:
        return self._shells

    @property
    def M_a(self) -> list[int]:
        """Start index of LCAO coefficients of each atom a

        The last element is the total number of LCAO coefficients
        """
        return self._M_a

    @property
    def setups_s(self) -> list[gpaw.setup.Setup]:
        return self._setups_s

    @classmethod
    def from_shells(cls, shells, setups: gpaw.setup.Setups):
        return cls(shells, setups)

    def symmetry_projectors_by_shell(self,
                                     l_sj: list[list[int]],
                                     permutations_sbo: list[np.ndarray]
                                     ) -> tuple[np.ndarray, np.ndarray]:
        """ Get symmetry projectors

        Parameters
        ----------
        permutations_sbo
            list of permutations of atoms in each shell, as returned by get_symmetry_permutations_by_shell

        l_sj
            list of orbital momentum numbers of basis functions in each shell

        Returns
        -------
        Tuple (U_siXmx, proj_siXX)

        """
        U_siXx, proj_siXX = zip(*[shell.symmetry_projectors_general(l_j, P_bo)
                                  for shell, l_j, P_bo in zip(self.shells, l_sj, permutations_sbo)])

        # X is the number of basis function in each shell
        nX_s = [U_iXx[0].shape[0] for U_iXx in U_siXx]

        assert all([np.sum([U_Xx.shape[1] for U_Xx in U_iXx]) == nX for (U_iXx, nX) in zip(U_siXx, nX_s)]), \
            'Must have consistent number of basis functions in each shell'

        return U_siXx, proj_siXX

    def global_LCAO_indices(self, a) -> list[int]:
        """ List of global LCAO indices corresponding to global atom a """
        Mstart = self.M_a[a]
        Mstop = self.M_a[a + 1]  # M_a is ordered in global atoms index, so we use a to index it
        return list(range(Mstart, Mstop))

    def get_coefficients_overlaps_by_shell(self,
                                           C_nM: np.ndarray,
                                           S_MM: np.ndarray
                                           ) -> tuple[list[list[int]],
                                                      list[np.ndarray],
                                                      list[np.ndarray],
                                                      list[np.ndarray]]:
        """ Extract the coefficients and overlap matrix elements for each shell

        Parameters
        ----------
        atomindex_sb
            List of indexes in each shell s

            atomindex_b = atomindex_sb[s] are the indices of atoms atoms in shell s
            b corresponds to a local atom index in each shell

        M_a
            List of starting M-indices by atom a in full system
        C_nM
            LCAO coefficients of full system

        S_MM
            Overlap matrix elements of full system

        Returns
        -------
        (x_sb, C_snx, S_sxM)

        x_sb[s] are the starting x-indices by local atom b in each shell s
        C_snx[s] are the LCAO coefficients of shell s
        S_sxM[s] is the overlap matrix of shell s with second index global
        """
        x_sb = []
        C_snx = []
        S_sxM = []

        for shell in self.shells:
            atomindex_b = shell.a_b

            # atomindex_b are the global atom indices, indexed by local shell index
            M_x = [M for a in atomindex_b for M in self.global_LCAO_indices(a)]
            C_nx = C_nM[:, M_x]
            S_xM = S_MM[M_x, :]

            # Number of basis functions of each local atom b
            Nx_b = [len(self.global_LCAO_indices(a)) for a in atomindex_b]
            # Start and stop of basis functions in each shell
            x_b = np.zeros(len(Nx_b) + 1, dtype=int)
            np.cumsum(Nx_b, out=x_b[1:])

            C_snx.append(C_nx)
            S_sxM.append(S_xM)
            x_sb.append(x_b)

        return x_sb, C_snx, S_sxM


class PointGroupBaseClass:
    def __init__(self, name, verbose=True):
        """ Point group base class

        All subclasses of this should be immutable
        """
        self.name = name

        # A set of 3x3 unitary matrices for each op
        self.ops_occ = None

        # Multiplication table for group
        self.mul_oo = None

        # Inverse table for group
        self.inv_oo = None

        # Classwise differentiation of operations
        self.ops_g = None

        # Classwise differentiation of operations
        self.g_o = None  # To which class g an op o belongs to

        # Character table
        self.character_ig = None

        # Names of the irreducible representations
        self.names_i = None

        # Names of symmetry classes
        self.names_g = None

        # Eigenspaces of each irreducible representation
        self.groups_i = None

        self.verbose = verbose

        self._build_ops(name)
        if verbose:
            self.print_ops()
        self._build_multiplication_table()
        self._find_conjugacy_classes()
        self._build_character_table()
        self._detect_conjugacy_classes()
        self._detect_irreps()
        self._name_groups_and_classes()

        self.order = len(self.ops_occ)

    def Ni(self):
        return len(self.groups_i)

    def Ng(self):
        return len(self.ops_g)

    def No(self):
        return self.order

    def __repr__(self):
        return self.name

    def symmetry_projectors(self, permutations_Xo):
        character_ig = self.character_ig
        I_XX = np.eye(len(permutations_Xo))
        U_iXx = []
        for i in range(self.Ni()):
            proj_XX = 0
            for g, ops_o in enumerate(self.ops_g):
                chi = character_ig[i, g]
                for o in ops_o:
                    P_XX = I_XX[permutations_Xo[:, o], :]  # Create a permutation matrix
                    proj_XX = proj_XX + chi * P_XX.T

            # Find the range of matrix
            eps_n, psi_Xn = np.linalg.eig(proj_XX)
            idx, = np.where(eps_n > 1e-4)
            q, r = np.linalg.qr(psi_Xn[:, idx])
            assert q.size == 0 or np.max(np.abs(q.imag)) < 1e-10
            q = q.real
            U_iXx.append(q)
        return U_iXx

    def _build_ops(self, name):
        raise NotImplementedError

    def _name_groups_and_classes(self):
        raise NotImplementedError

    def print_ops(self, ops_occ=None, columns=8):
        if ops_occ is None:
            ops_occ = self.ops_occ
        rows = int(np.ceil(len(ops_occ) / columns))
        for row in range(rows):
            for c in range(3):
                for col in range(columns):
                    idx = row * columns + col
                    if idx >= len(ops_occ):
                        continue
                    op_cc = ops_occ[idx]
                    print("(%2d %2d %2d)     " % (op_cc[c, 0], op_cc[c, 1], op_cc[c, 2]), end="")
                print()
            print()
        print('Total symmetries: %d' % len(ops_occ))

    def get_op_id(self, op_cc):
        for o1, op1_cc in enumerate(self.ops_occ):
            if np.linalg.norm(op_cc - op1_cc) < 1e-8:
                return o1
        raise ValueError("Unknown operation: %s." % str(op_cc))

    def _build_multiplication_table(self):
        ops_occ = self.ops_occ
        N = len(ops_occ)
        mul_oo = np.zeros((N, N), dtype=int)
        inv_oo = np.zeros((N, N), dtype=int)
        for o1, op1_cc in enumerate(ops_occ):
            for o2, op2_cc in enumerate(ops_occ):
                o3 = self.get_op_id(np.dot(op1_cc, op2_cc))
                mul_oo[o1, o2] = o3
                inv_oo[o1, o3] = o2
        self.mul_oo, self.inv_oo = mul_oo, inv_oo

    def print_multiplication_table(self):
        np.set_printoptions(suppress=True, edgeitems=48, linewidth=200)
        print("Multiplication table:")
        print(self.mul_oo)

    def _find_conjugacy_classes(self):
        ops_occ = self.ops_occ
        N = len(ops_occ)
        op_pool = range(N)
        self.ops_g = []

        self.g_o = np.zeros((N,), int)  # Conjugacy class index for each op
        while True:
            # Loop until all operations are assigned a class
            if len(op_pool) == 0:
                break
            # Take an element from operation pool...
            op1 = op_pool[0]
            op1_cc = ops_occ[op1]
            # ...conjugate it with all possible operations, see the result, and remove any duplicates
            conjugacy_class = np.unique([self.get_op_id(np.dot(op2_cc, np.dot(op1_cc, op2_cc.T))) for o2, op2_cc in enumerate(ops_occ)])

            # Fill g_o array that maps ops to class
            for op in conjugacy_class:
                self.g_o[op] = len(self.ops_g)
            self.ops_g.append(conjugacy_class)
            # Remove all operations already assigned a class from the pool
            op_pool = list(set(op_pool) - set(conjugacy_class))

        if self.verbose:
            print("Found %d conjugacy classes" % len(self.ops_g))

    def diagonalize_and_group(self, H_oo):
        eps, psi = np.linalg.eigh(H_oo)
        groups = np.unique(abs(eps[:, None] - eps[None, :]) < 1e-6, axis=0)
        if self.verbose:
            print("Found %d irreducible representations." % len(groups))

        # Add eigenvales to lists
        groups_i = [[] for i in range(len(groups))]
        for irrep, eig_idx in zip(*np.where(groups)):
            groups_i[irrep].append(psi[:, eig_idx])

        return [np.array(x) for x in groups_i]

    def _build_character_table(self):
        ops_occ = self.ops_occ
        N = len(ops_occ)

        # Diagonalize arbitrary Hamiltonian (the form 1/(1+g) is irrelevant)
        # to numerically build the character table. The degenerate eigenspaces
        # describe the irreducible representations.
        # There might be an accidental degeneracy, in which case the a representation
        # might end up being a direct product of two representations.
        H_oo = 1 / (self.g_o[self.inv_oo] + 1)
        groups_i = self.diagonalize_and_group(1 / (self.g_o[self.inv_oo] + 1))
        self.groups_i = groups_i
        character_ig = np.zeros((len(groups_i), len(self.ops_g)), dtype=int)

        # For each group...
        for i, psi_no in enumerate(groups_i):
            # For each conjugacy class...
            for g, ops in enumerate(self.ops_g):
                # It is not necessary to loop over all group operations. However, it will be a good sanity check.
                # Calculate the trace of this irrep under operation o
                traces_o = np.array([np.trace(np.dot(psi_no[:, self.mul_oo[:, o]], psi_no.T.conjugate())) for o in ops])
                h = len(psi_no)**0.5
                assert(np.all(abs(traces_o - traces_o[0]) < 1e-10))
                character_ig[i, g] = int(np.round(traces_o[0] / h))
                #print(int(np.round(traces_o[0])) - traces_o[0])
                #assert(abs(int(np.round(traces_o[0])) - traces_o[0]) < 1e-10)

        self.character_ig = character_ig

    def _detect_conjugacy_class(self, ops_occ):
        det_o = np.array([np.linalg.det(op_cc) for op_cc in ops_occ])
        eigs_o = np.array([np.sort(np.linalg.eig(op_cc)[0]) for op_cc in ops_occ])
        if len(det_o) == 1 and np.all(np.isclose(eigs_o, [-1, -1, 1])):
            return "C2"
        if len(det_o) == 1 and np.all(np.isclose(eigs_o, [-1, -1, -1])):
            return "i"  # Inversion flips all axes, -x, -y, -z
        if len(det_o) == 1 and np.all(np.isclose(eigs_o, [1, 1, 1])):
            return "E"  # Identity leaves all axes intact x, y, z
        if len(det_o) == 3 and np.all(np.isclose(eigs_o, [-1, -1, 1])):
            return "3C2"  # C2 rotation along z is -x, -y, z
        if len(det_o) == 6 and np.all(np.isclose(eigs_o, [-1, -1, 1])):
            return "6C2"
        if len(det_o) == 6 and np.all(np.isclose(eigs_o, [-1, -1j, 1j])):
            return "6S4"  # -1j and 1j corresponds to 90 rotation. Determinant is -1 thus, improper.
        if len(det_o) == 3 and np.all(np.isclose(eigs_o, [-1, 1, 1])):
            return "3sh"  # Horizontal mirror operation flips one of the coordinates
        if len(det_o) == 6 and np.all(np.isclose(eigs_o, [-1, 1, 1])):
            return "6sd"
        if len(det_o) == 8 and np.all(np.isclose(eigs_o, [-1, np.exp(-1j * 2 * np.pi / 6), np.exp(1j * 2 * np.pi / 6)])):
            return "8S6"
        if len(det_o) == 6 and np.all(np.isclose(eigs_o, [-1j, 1j, 1])):
            return "6C4"
        if len(det_o) == 8 and np.all(np.isclose(eigs_o, [np.exp(-1j * np.pi * 2 / 3), np.exp(1j * np.pi * 2 / 3), 1])):
            return "8C3"
        return "bug?"

    def _detect_conjugacy_classes(self):
        self.names_g = [self._detect_conjugacy_class([self.ops_occ[o] for o in classops]) for classops in self.ops_g]

    def class_id(self, classname):
        try:
            return self.names_g.index(classname)
        except ValueError:
            return None

    def _detect_irreps(self):
        self.names_i = [self._detect_irrep(self.character_ig[i, :]) for i in range(self.character_ig.shape[0])]

    def print_character_table(self, class_order, irrep_order):
        try:
            print("%-20s" % "irreps/classes", end="")
            if class_order is None:
                class_order = self.names_g
            if irrep_order is None:
                irrep_order = self.names_i

            for name in class_order:
                print("%-5s" % name, end="")
            print()
            for iname in irrep_order:
                i = self.names_i.index(iname)
                print("%-20s" % self.names_i[i], end="")
                for gname in class_order:
                    g = self.names_g.index(gname)
                    print("%-5s" % ("%+02d" % self.character_ig[i, g]), end="")
                print()
        except ValueError:
            print('...')


class C1Symmetry(PointGroupBaseClass):
    def __init__(self, verbose=True):
        PointGroupBaseClass.__init__(self, 'C1', verbose=verbose)

    def _build_ops(self, name):
        self.ops_occ = [np.eye(3)]

    def _detect_irrep(self, signature):
        assert(len(signature) == 1 and signature[0] == 1)
        return "A1g"

    def _name_groups_and_classes(self):
        if self.verbose:
            self.print_character_table(class_order=['E'],
                                       irrep_order=['A1g'])


class C2Symmetry(PointGroupBaseClass):
    def __init__(self, verbose=True):
        PointGroupBaseClass.__init__(self, 'C2', verbose=verbose)

    def _build_ops(self, name):
        self.ops_occ = [ np.eye(3), np.array([[-1,0,0],[0,-1,0],[0,0,1]]) ] # E and C2

    def _detect_irrep(self, signature):
        if len(signature)==2 and np.all(signature == np.array([1,1])):
            return "A"
        if len(signature)==2 and np.all(signature == np.array([1,-1])):
            return "B"
        return "unknown"

    def _name_groups_and_classes(self):
        if self.verbose:
            self.print_character_table(class_order = ['E','C2'],
                                       irrep_order = ['A','B'])

#
# Positions:
#    0 O      3.000000    3.763239    3.596309    ( 0.0000,  0.0000,  0.0000)
#    1 H      3.000000    4.526478    3.000000    ( 0.0000,  0.0000,  0.0000)
#    2 H      3.000000    3.000000    3.000000    ( 0.0000,  0.0000,  0.0000)
# C2 axis is z
# sigma plane is yz
# sigma' plane is xz

class C2vSymmetry(PointGroupBaseClass):
    def __init__(self, verbose=True):
        PointGroupBaseClass.__init__(self, 'C2v', verbose=verbose)


    def _build_ops(self, name):
        self.ops_occ = [ np.eye(3),                                # E
                         np.array([[-1,0,0],[0,-1,0],[0,0, 1]]),   # C2 (rotate z 180)
                         np.array([[-1,0,0],[0, 1,0],[0,0, 1]]),   # sigma (mirror yz, i.e. flip x)
                         np.array([[ 1,0,0],[0,-1,0],[0,0, 1]]) ]  # sigma' (mirror xz i.e. flip y)

    def _detect_irrep(self, signature):
        if len(signature)==2 and np.all(signature == np.array([1,1])):
            return "A"
        if len(signature)==2 and np.all(signature == np.array([1,-1])):
            return "B"
        return "unknown"

    def _name_groups_and_classes(self):
        if self.verbose:
            self.print_character_table(class_order = ['E','C2','sigma','sigma'''],
                                       irrep_order = ['A1','A2','B1','B2'])

class OhSymmetry(PointGroupBaseClass):
    def __init__(self, verbose=True):
        PointGroupBaseClass.__init__(self, 'Oh', verbose=verbose)

    def _build_ops(self, name):
        assert(name == 'Oh')
        if self.verbose:
            print('create_symmetry operations:')
        def gen_ops(elements=[]):
            if len(elements) < 9:
                for value in [-1, 0, 1]:
                    yield from gen_ops(elements + [ value ])
            else:
                op_cc = np.array(elements).reshape((3,3))
                # Check if unitary
                if np.linalg.norm(np.dot(op_cc.T, op_cc)-np.eye(3))<1e-5:
                    yield op_cc

        self.ops_occ = [ x for x in gen_ops() ]
        assert(len(self.ops_occ) == 48)

    def _find_conjugacy_classes(self):
        PointGroupBaseClass._find_conjugacy_classes(self)
        sizes = np.sort(list(map(len, self.ops_g)))
        assert(np.all(sizes == np.sort([1, 8, 6, 6, 3, 1, 6, 8, 3, 6])))

    def _detect_irrep(self, signature):
        h = signature[ self.class_id("E") ]

        #rotations = [ self.class_id(name) for name in self.names_g if ("C" in name) ]
        rotations = self.class_id("6C2")
        ug = "g" if signature[ self.class_id("i") ] > 0 else "u"
        C = "?"
        N = ""
        if signature[self.class_id("6C4")]>0:
            N = "1"
        else:
            N = "2"
        if h == 1:
            C = "A"
        if h == 2:
            C = "E"
            N = ""
        if h == 3:
            C="T"

        return C+N+ug

    def _name_groups_and_classes(self):
        if self.verbose:
            self.print_character_table(class_order = ['E','8C3','6C2','6C4','3C2','i','6S4','8S6','3sh','6sd'],
                                       irrep_order = ['A1g','A2g','Eg','T1g','T2g','A1u','A2u','Eu','T1u','T2u'])

        # target_ig = np.array([[ 1,  1,  1,  1,  1,  1,  1,  1,  1,  1 ],
        #                       [ 1,  1, -1, -1,  1,  1, -1,  1,  1, -1 ],
        #                       [ 2, -1,  0,  0,  2,  2,  0, -1,  2,  0 ],
        #                       [ 3,  0, -1,  1, -1,  3,  1,  0, -1, -1 ],
        #                       [ 3,  0,  1, -1, -1,  3, -1,  0, -1,  1 ],
        #                       [ 1,  1,  1,  1,  1, -1, -1, -1, -1, -1 ],
        #                       [ 1,  1, -1, -1,  1, -1,  1, -1, -1,  1 ],
        #                       [ 2, -1,  0,  0,  2, -2,  0,  1, -2,  0 ],
        #                       [ 3,  0, -1,  1, -1, -3, -1,  0,  1,  1 ],
        #                       [ 3,  0,  1, -1, -1, -3,  1,  0,  1, -1 ]])
        # target_class_sizes = np.array([ 1, 8, 6, 6, 3, 1, 6, 8, 3, 6 ])


def Cn_cc(n=1, power=1):
    angle = 2 * np.pi / n * power
    s, c = np.sin(angle), np.cos(angle)
    return np.array([[c, s, 0],
                     [-s, c, 0],
                     [0, 0, 1]])


class C3Symmetry(PointGroupBaseClass):
    def __init__(self, verbose=True):
        PointGroupBaseClass.__init__(self, 'C3', verbose=verbose)

    def _build_ops(self, name):
        assert(name == 'C3')
        self.ops_occ = []
        for power in range(3):
            self.ops_occ.append(Cn_cc(n=3, power=power))
        assert(len(self.ops_occ) == 3)

    def _find_conjugacy_classes(self):
        PointGroupBaseClass._find_conjugacy_classes(self)
        # sizes = np.sort(list(map(len, self.ops_g)))
        # assert(np.all(sizes == np.sort([1, 8, 6, 6, 3, 1, 6, 8, 3, 6])))

    def _detect_irrep(self, signature):
        # print(signature)
        return '???'

    def _name_groups_and_classes(self):
        if self.verbose:
            self.print_character_table()


class C6Symmetry(PointGroupBaseClass):
    def __init__(self, verbose=True):
        PointGroupBaseClass.__init__(self, 'C6', verbose=verbose)

    def _build_ops(self, name):
        assert(name == 'C6')
        self.ops_occ = []
        for power in range(6):
            self.ops_occ.append(Cn_cc(n=6, power=power))
        assert(len(self.ops_occ) == 6)

    def _find_conjugacy_classes(self):
        PointGroupBaseClass._find_conjugacy_classes(self)
        # sizes = np.sort(list(map(len, self.ops_g)))
        # assert(np.all(sizes == np.sort([1, 8, 6, 6, 3, 1, 6, 8, 3, 6])))

    def _detect_irrep(self, signature):
        return '?irrep?'

    def _name_groups_and_classes(self):
        if self.verbose:
            self.print_character_table()
