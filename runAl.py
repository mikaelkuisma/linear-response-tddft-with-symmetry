from gpaw import GPAW
from sys import argv
from ase.io import read

calculation_parameters = {'mode': 'lcao', 'xc': 'LDA', 'basis': 'sz(dzp)', 'txt': '-',
                          'nbands': 'nao', 'h': 0.2, 'convergence': {'density': 1e-8},
                          'poissonsolver': {'name': 'MomentCorrectionPoissonSolver',
                                            'poissonsolver': 'fast',
                                            'moment_corrections': 1 + 3 + 5}}


def get_atoms():
    atoms = read(argv[1])
    atoms.center(vacuum=4)
    return atoms


if __name__ == "__main__":
    atoms = get_atoms()

    N = len(atoms)
    charge = 0
    if N == 1:
        charge = +1
    if N == 79:
        charge = -1
    if N == 13:
        charge = -1
    if N == 43:
        charge = -1
    calculation_parameters.update(charge=charge)
    calc = GPAW(**calculation_parameters)
    atoms.set_calculator(calc)
    atoms.get_potential_energy()
    calc.write(argv[1] + '.gpw', mode='all')
