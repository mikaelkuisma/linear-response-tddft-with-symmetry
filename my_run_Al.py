from gpaw import GPAW
from sys import argv
from ase.io import read
from gpaw.poisson import PoissonSolver

from gpaw.lcaotddft import LCAOTDDFT
from gpaw.lcaotddft.dipolemomentwriter import DipoleMomentWriter

calculation_parameters = { 'mode':'lcao', 'xc':'LDA', 'basis':'sz(dzp)', 'txt':'-', 
                           'nbands':'nao', 'h':0.25, 'convergence':{'density':1e-8},
                           'poissonsolver':PoissonSolver(remove_moment=1+3+5) }

def get_atoms():
    atoms = read(argv[1])
    atoms.center(vacuum=5)
    return atoms

if __name__ == "__main__":
    atoms = get_atoms()

    N = len(atoms)
    charge = 0
    if N == 1:
        charge = +1
    if N == 79:
        charge = -1
    if N == 13:
        charge = -1
    if N == 43:
        charge = -1
    calculation_parameters.update(charge = charge)
    calc = GPAW(**calculation_parameters)
    atoms.set_calculator(calc)
    atoms.get_potential_energy()
    gpw_file = argv[1]+'.gpw'
    calc.write(gpw_file, mode='all')

    td_calc = LCAOTDDFT(gpw_file, fxc='RPA')
    DipoleMomentWriter(td_calc, argv[1]+'.dm.dat')
    td_calc.absorption_kick([0.0, 0.0, 1e-5])
    td_calc.propagate(10, 3000)
    
    from gpaw.tddft.spectrum import photoabsorption_spectrum
    photoabsorption_spectrum(argv[1]+'.dm.dat', argv[1]+'.spec.dat')

