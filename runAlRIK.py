import numpy as np
from gpaw import GPAW, KohnShamConvergenceError
from gpaw.poisson import PoissonSolver
from gpaw.poisson_moment import MomentCorrectionPoissonSolver


def calculate_K_MMMM(atoms, calctxt='/dev/null'):
    """ Calculate the Coulomb tensor in the LCAO basis using the Resolution of identity technique

    The Coulomb tensor is

                          *             -1   *
    K         = ∫ dr dr' φ(r) φ(r) |r-r'|   φ(r') φ(r')
     M1M2M3M4             M1   M2            M3    M4
    """
    xc = 'EXX'
    alg = 'RI-R'

    params = {'mode': 'lcao', 'xc': 'LDA', 'basis': 'sz(dzp)', 'txt': calctxt,
              'nbands': 'nao', 'h': 0.2, 'convergence': {'density': 1e-8}}
    params.update(xc='%s:backend=aux-lcao:threshold=1e-9:algorithm=%s' % (xc, alg),
                  maxiter=2)

    params.update(poissonsolver=MomentCorrectionPoissonSolver(PoissonSolver(),
                                                              moment_corrections=1 + 3 + 5))

    calc = GPAW(**params)
    atoms.calc = calc

    try:
        atoms.get_potential_energy()
    except KohnShamConvergenceError:
        pass

    K_MMMM = calc.hamiltonian.xc.ri_algorithm.get_full_K_MMMM()

    return calc, K_MMMM


def fast_calculate_K(atoms, verbose=False, calctxt='/dev/null'):
    """ Calculate the Coulomb tensor using the Resolution of identity technique

        The Coulomb tensor is

                            *             -1   *
        K       = ∫ dr dr' ψ(r) ψ(r) |r-r'|   ψ(r') ψ(r')
         iai'a'             i    a             i'    a'
    """
    calc, K_MMMM = calculate_K_MMMM(atoms, calctxt=calctxt)

    C_nM = calc.wfs.kpt_u[0].C_nM
    if verbose:
        print('N^5 rotation')
    K_nnnn = np.einsum('MNOP,Mi,Nj,Ok,Pl->ijkl', K_MMMM, C_nM, C_nM, C_nM, C_nM, optimize=True)

    if verbose:
        print('Done')
        print(K_MMMM.shape)

    return K_nnnn


if __name__ == '__main__':
    from runAl import get_atoms

    atoms = get_atoms()
    fast_calculate_K(atoms, verbose=True, calctxt='-')
