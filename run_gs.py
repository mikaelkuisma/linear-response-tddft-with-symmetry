#!/usr/bin/env python3
import click

import numpy as np

from gpaw import GPAW
from gpaw.external import ExternalPotential


calculation_parameters = {'mode': 'lcao', 'xc': 'LDA', 'basis': 'sz(dzp)', 'txt': '-',
                          'nbands': 'nao', 'h': 0.2, 'convergence': {'density': 1e-8},
                          'poissonsolver': {'name': 'MomentCorrectionPoissonSolver',
                                            'poissonsolver': 'fast',
                                            'moment_corrections': 1 + 3 + 5}}


class ShiftAngularMomentumChannelsPotential(ExternalPotential):

    def __init__(self, shift_ai):
        from gpaw.utilities import pack2

        self.shift_ai = shift_ai
        self.dH_ap = [pack2(np.diag(shift_i)) for shift_i in shift_ai]
        self.a = 0  # XXX Ugly hack to keep track of which atom
        self.Na = len(self.dH_ap)

    @classmethod
    def from_shift_spd(cls, calc, atoms, shifts, shiftp, shiftd):
        from ase.units import Hartree

        calc.initialize(atoms)
        calc.set_positions(atoms)
        shift_l = np.array([shifts, shiftp, shiftd, 0, 0, 0, 0]) / Hartree

        l_aj = [setup.l_j for setup in calc.setups]
        shift_ai = [[shift_l[l] for l in l_j for _ in range(2 * l + 1)] for l_j in l_aj]

        self = cls(shift_ai)
        calc.hamiltonian.vext = self
        calc.parameters.external = self.todict()
        return self

    def calculate_potential(self, gd):
        self.vext_g = gd.zeros()

    def paw_correction(self, Delta_p, dH_sp) -> None:
        shift_dH_p = self.dH_ap[self.a]
        for dH_p in dH_sp:
            dH_p += shift_dH_p
        self.a = (self.a + 1) % self.Na

    def todict(self):
        return {'name': self.__class__.__name__, 'shift_ai': self.shift_ai}


def register_custom_external():
    from gpaw.external import known_potentials

    for cls in [ShiftAngularMomentumChannelsPotential]:
        known_potentials[cls.__name__] = cls


def get_atoms(xyzfile):
    from ase.io import read

    atoms = read(xyzfile)
    atoms.center(vacuum=4)
    return atoms


@click.command()
@click.argument('xyzfile')
@click.option('--shifts', required=False, type=float, default=0, help='Introduce a rigid shift (in eV) to the s angular momentum channels')
@click.option('--shiftp', required=False, type=float, default=0, help='Introduce a rigid shift (in eV) to the p angular momentum channels')
@click.option('--shiftd', required=False, type=float, default=0, help='Introduce a rigid shift (in eV) to the d angular momentum channels')
@click.option('--output', default=None, required=False)
def run(xyzfile, shifts, shiftp, shiftd, output):
    atoms = get_atoms(xyzfile)

    if output is None:
        gpwfile = xyzfile.removesuffix('.xyz') + '.gpw'
    else:
        gpwfile = output

    N = len(atoms)
    charge = 0
    if N == 1:
        charge = +1
    if N == 79:
        charge = -1
    if N == 13:
        charge = -1
    if N == 43:
        charge = -1
    calculation_parameters.update(charge=charge)
    calc = GPAW(**calculation_parameters)
    atoms.set_calculator(calc)
    if shifts != 0 or shiftp != 0 or shiftd != 0:
        ShiftAngularMomentumChannelsPotential.from_shift_spd(calc, atoms, shifts, shiftp, shiftd)
    atoms.get_potential_energy()
    calc.write(gpwfile, mode='all')


if __name__ == "__main__":
    run()
