import os
import sys

from contextlib import contextmanager

import numpy as np

import pytest
from ase.build import molecule
from gpaw.mpi import broadcast, world

sys.path.insert(0, '.')

from symmetry import AtomsRepresentation  # noqa:


@contextmanager
def execute_in_tmp_path(request, tmp_path_factory):
    if world.rank == 0:
        # Obtain basename as
        # * request.function.__name__  for function fixture
        # * request.module.__name__    for module fixture
        basename = getattr(request, request.scope).__name__
        path = tmp_path_factory.mktemp(basename)
    else:
        path = None
    path = broadcast(path)
    cwd = os.getcwd()
    os.chdir(path)
    try:
        yield path
    finally:
        os.chdir(cwd)


@pytest.fixture(scope='function')
def in_tmp_dir(request, tmp_path_factory):
    """Run test function in a temporary directory."""
    with execute_in_tmp_path(request, tmp_path_factory) as path:
        yield path


def float_equal(a, b, precision=1e-8):
    assert np.abs(a - b) < precision, f'a {a} != b {b}'


@pytest.mark.parametrize('atoms, target_point_group', [
    (molecule('C6H6'), 'C6'),  # TODO change to D6h once it is implemented
    (molecule('H2O'), 'C2v'),
    (molecule('H2'), 'C6'),  # TODO change to D6h (actually Dinfh)
    (molecule('N'), 'Oh'),
])
def test_point_group(atoms, target_point_group):
    atoms_rep = AtomsRepresentation.from_atoms(atoms)
    assert atoms_rep.point_group.name == target_point_group, f'{atoms_rep} != {target_point_group} for {atoms}'
