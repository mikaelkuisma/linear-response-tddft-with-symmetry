import os
import sys

from contextlib import contextmanager

import numpy as np

import pytest
from ase import Atoms
from gpaw import GPAW
from gpaw.mpi import broadcast, world

sys.path.insert(0, '.')

from lrtddft3 import LCAOSymmetry, LRTDDFT3  # noqa: E402
from runAlRIK import calculate_K_MMMM, fast_calculate_K  # noqa: E402


@contextmanager
def execute_in_tmp_path(request, tmp_path_factory):
    if world.rank == 0:
        # Obtain basename as
        # * request.function.__name__  for function fixture
        # * request.module.__name__    for module fixture
        basename = getattr(request, request.scope).__name__
        path = tmp_path_factory.mktemp(basename)
    else:
        path = None
    path = broadcast(path)
    cwd = os.getcwd()
    os.chdir(path)
    try:
        yield path
    finally:
        os.chdir(cwd)


@pytest.fixture(scope='function')
def in_tmp_dir(request, tmp_path_factory):
    """Run test function in a temporary directory."""
    with execute_in_tmp_path(request, tmp_path_factory) as path:
        yield path


def float_equal(a, b, precision=1e-8):
    assert np.abs(a - b) < precision, f'a {a} != b {b}'


@pytest.fixture
def calc(request):
    from gpaw.poisson import PoissonSolver
    from gpaw.poisson_moment import MomentCorrectionPoissonSolver

    param = request.param
    if isinstance(param, str):
        calc = GPAW(param)
    elif isinstance(param, Atoms):
        atoms = param
        atoms.center(vacuum=4)
        ps = MomentCorrectionPoissonSolver(PoissonSolver(),
                                           moment_corrections=1 + 3 + 5)
        calc = GPAW(mode='lcao', basis='sz(dzp)', h=0.25, poissonsolver=ps, txt='/dev/null')
        atoms.calc = calc
        atoms.get_potential_energy()
    else:
        raise ValueError(f'Don\'t know what to do with type {type(param)}')

    return calc


@pytest.fixture
def tddft3(calc):
    sym = LCAOSymmetry(calc)
    sym.construct_full_from_shells()
    sym.analyze_ground_state_symmetry()
    tddft3 = LRTDDFT3(sym, irreps=[0])

    return tddft3


@pytest.mark.parametrize('atoms', [
    Atoms('H2', positions=[(0, 0, 0), (5, 0, 0)]),
    Atoms('H2', positions=[(0, 0, 0), (6, 0, 0)]),
])
def test_hydrogen_K_MMMM(atoms):
    from ase.units import Bohr

    atoms = atoms.copy()
    atoms.center(vacuum=4)

    _, K_MMMM = calculate_K_MMMM(atoms)
    distance = atoms.get_distance(0, 1) / Bohr

    assert K_MMMM[0, 0, 1, 1] == K_MMMM[1, 1, 0, 0]
    float_equal(K_MMMM[0, 1, 0, 1], 0, precision=1e-3)
    float_equal(np.max(np.abs(K_MMMM[0, 1, ...])), 0, precision=1e-4)

    # No overlap -> Should be close to 1/r
    float_equal(K_MMMM[0, 0, 1, 1], 1 / distance, precision=1e-3)


@pytest.mark.parametrize('calc', [
    Atoms('H2', positions=[(0, 0, 0), (5, 0, 0)]),
    Atoms('H2', positions=[(0, 0, 0), (6, 0, 0)]),
], indirect=True)
def test_H2_fast_calculate_K(calc, tddft3):
    import itertools

    calc = calc
    atoms = calc.atoms
    C_nM = calc.wfs.collect_array('C_nM', 0, 0)

    _, K_MMMM = calculate_K_MMMM(atoms)
    K_nnnn = fast_calculate_K(atoms)
    ref_K_nnnn = np.zeros_like(K_nnnn)

    for n1, n2, n3, n4 in itertools.product(range(2), repeat=4):
        occC1_Mn = C_nM[n1:n1 + 1].T
        unoccC1_Mn = C_nM[n2:n2 + 1].T
        vec1_nn = np.array([1])
        occC2_Mn = C_nM[n3:n3 + 1].T
        unoccC2_Mn = C_nM[n4:n4 + 1].T
        vec2_nn = np.array([1])

        rho1_g, D1_ap = tddft3.charge_density(occC1_Mn, unoccC1_Mn, vec1_nn)
        VHt1_g = calc.density.finegd.zeros()
        calc.hamiltonian.poisson.solve(VHt1_g, rho1_g, charge=None)

        H_ap = {}
        for a, D1_p in D1_ap.items():
            C_pp = calc.wfs.setups[a].M_pp
            H_ap[a] = 2.0 * np.dot(C_pp, D1_p)

        rho2_g, D2_ap = tddft3.charge_density(occC2_Mn, unoccC2_Mn, vec2_nn)
        K = calc.density.finegd.integrate(VHt1_g * rho2_g)
        for a, D2_p in D2_ap.items():
            # print("Corr", np.dot(D2_p, H_ap[a]))
            K = K + np.dot(D2_p, H_ap[a])
        ref_K_nnnn[n1, n2, n3, n4] = K

    ref_K = K
    test_K = K_nnnn[n1, n2, n3, n4]

    assert np.abs((ref_K - test_K) / ref_K) < 0.03, f'ref {ref_K}, test {test_K}'


@pytest.mark.parametrize('calc', [
    Atoms('H2', positions=[(0, 0, 0), (5, 0, 0)]),
], indirect=True)
def test_consistent_K_small_system(calc):
    """ Test that the K matrices by slow calculation and fast calculation
    are close. Calculate all values for all irreps"""
    sym = LCAOSymmetry(calc)
    sym.construct_full_from_shells()
    sym.analyze_ground_state_symmetry()
    tddft3 = LRTDDFT3(sym)
    out_ref = tddft3.calculate()
    out_test = tddft3.calculate_fast()

    for key in ['delta_N', 'mu_Nv']:
        assert np.allclose(out_ref[key], out_test[key])

    key = 'K_NN'
    assert np.allclose(out_ref[key], out_test[key], rtol=0.05)


@pytest.mark.parametrize('calc', [
    'Al13.xyz.gpw',
], indirect=True)
def test_consistent_K_large_system(calc, tddft3):
    """ Test that the K matrices by slow calculation and fast calculation
    are close. Calculate only first values for irrep 9"""
    import itertools

    # This test will fail if the atoms are shuffled (if the center atom is not first or last)

    dE_max = 1  # eV
    out_ref = tddft3.calculate(dE_max=dE_max)
    out_test = tddft3.calculate_fast(dE_max=dE_max)

    for key in ['delta_N', 'mu_Nv']:
        assert np.allclose(out_ref[key], out_test[key])

    nN = len(out_ref['K_NN'])
    for N1, N2 in itertools.product(range(nN), repeat=2):
        float_equal(out_ref['K_NN'][N1, N2], out_test['K_NN'][N1, N2], 0.05)
