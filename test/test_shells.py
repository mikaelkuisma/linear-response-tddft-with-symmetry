import os
import sys

from contextlib import contextmanager

import numpy as np

import pytest
from ase import Atoms
from gpaw import GPAW
from gpaw.mpi import broadcast, world

sys.path.insert(0, '.')

from lrtddft3 import LCAOSymmetry, LRTDDFT3  # noqa: E402
from symmetry import AtomsRepresentation, Shells  # noqa: E402


def do_nothing(*args, **kwargs):
    pass


@contextmanager
def execute_in_tmp_path(request, tmp_path_factory):
    if world.rank == 0:
        # Obtain basename as
        # * request.function.__name__  for function fixture
        # * request.module.__name__    for module fixture
        basename = getattr(request, request.scope).__name__
        path = tmp_path_factory.mktemp(basename)
    else:
        path = None
    path = broadcast(path)
    cwd = os.getcwd()
    os.chdir(path)
    try:
        yield path
    finally:
        os.chdir(cwd)


@pytest.fixture(scope='function')
def in_tmp_dir(request, tmp_path_factory):
    """Run test function in a temporary directory."""
    with execute_in_tmp_path(request, tmp_path_factory) as path:
        yield path


def float_equal(a, b, precision=1e-8):
    assert np.abs(a - b) < precision, f'a {a} != b {b}'


@pytest.fixture
def calc(request):
    def calc_from_atoms(atoms, **calc_kwargs):
        atoms.center(vacuum=4)
        params = dict(poissonsolver={'name': 'MomentCorrectionPoissonSolver',
                                     'poissonsolver': 'fast',
                                     'moment_corrections': 1 + 3 + 5},
                      mode='lcao', basis='sz(dzp)', h=0.25,
                      txt='/dev/null')
        params.update(**calc_kwargs)
        calc = GPAW(**params)
        atoms.calc = calc
        atoms.get_potential_energy()
        return calc

    param = request.param
    if isinstance(param, str):
        calc = GPAW(param)
    elif isinstance(param, Atoms):
        calc = calc_from_atoms(param)
    elif isinstance(param, dict):
        calc = calc_from_atoms(param['atoms'], **param['calc_kwargs'])
    else:
        raise ValueError(f'Don\'t know what to do with type {type(param)}')

    return calc


@pytest.fixture
def tddft3(calc):
    sym = LCAOSymmetry(calc)
    sym.construct_full_from_shells()
    sym.analyze_ground_state_symmetry()
    tddft3 = LRTDDFT3(sym, irreps=[0])

    return tddft3


def symmetry_operator_sptype(self, permutations_Xo, o):
    # gpaw has y z x
    map_mm = np.array([[1.0, 0.0, 0.0, 0.0],
                       [0.0, 0.0, 1.0, 0.0],
                       [0.0, 0.0, 0.0, 1.0],
                       [0.0, 1.0, 0.0, 0.0]]).T
    I_XX = np.eye(len(permutations_Xo))
    P_XX = I_XX[permutations_Xo[:, o], :]  # Create a permutation matrix
    op_mm = np.zeros((4, 4))
    op_mm[0, 0] = 1.0
    op_mm[1:4, 1:4] = self.ops_occ[o].T
    op_mm = np.dot(np.dot(map_mm.T, op_mm), map_mm)
    P_XmXm = np.kron(P_XX, op_mm).T
    return P_XmXm


def symmetry_projectors_sptype(self, permutations_Xo):
    proj_iMM = []
    # gpaw has s py pz px
    map_mm = np.array([[1.0, 0.0, 0.0, 0.0],
                       [0.0, 0.0, 1.0, 0.0],
                       [0.0, 0.0, 0.0, 1.0],
                       [0.0, 1.0, 0.0, 0.0]]).T
    # In addition, that permutations_Xo tells us that atom 3 goes to atom 2, when operatio o is performed
    # We need to evaluate permutations_mmo (which is readily from self.ops_occ)
    character_ig = self.character_ig
    I_XX = np.eye(len(permutations_Xo))
    U_iXmx = []
    for i in range(self.Ni()):
        proj_XmXm = 0
        for g, ops_o in enumerate(self.ops_g):
            chi = character_ig[i, g]
            for o in ops_o:
                P_XX = I_XX[permutations_Xo[:, o], :]  # Create a permutation matrix
                # Here, one implicitly assumes that each atom has sppp orbitals.
                op_mm = np.zeros((4, 4))
                op_mm[0, 0] = 1.0
                op_mm[1:4, 1:4] = self.ops_occ[o].T
                # spd
                op_mm = np.dot(np.dot(map_mm.T, op_mm), map_mm)
                # P_XmXm = np.kron(op_mm, P_XX)
                P_XmXm = np.kron(P_XX, op_mm)
                proj_XmXm = proj_XmXm + chi * P_XmXm.T
        proj_XmXm *= self.character_ig[i, -1] * 1.0 / self.order  # XXX Hard coded

        # Find the range of matrix
        eps_n, psi_Xmn = np.linalg.eig(proj_XmXm)
        # for n in eps_n:
        #     print("%.5f +1j%.5f" % (n.real, n.imag),end=' ')
        # print()
        idx, = np.where(abs(eps_n) > 1e-4)
        q, r = np.linalg.qr(psi_Xmn[:, idx])
        assert q.size == 0 or np.max(np.abs(q.imag)) < 1e-10
        q = q.real
        # q = psi_Xmn[:, idx]
        U_iXmx.append(q)
        proj_iMM.append(proj_XmXm)

    return U_iXmx, proj_iMM


sloppy_convergence = dict(convergence={'density': 1e-3}, nbands='nao', poissonsolver={'name': 'fast'})


@pytest.mark.parametrize('calc', [
    dict(atoms=Atoms('H2Na', positions=[(0, 0, 0), (2, 0, 0), (1, 0, 0)]),
         calc_kwargs=sloppy_convergence),
    dict(atoms=Atoms('Na3', positions=[(0, 0, 0), (1, 0, 0), (2, 0, 0)]),
         calc_kwargs=sloppy_convergence),
    dict(atoms=Atoms('Na3', positions=[(0, 0, 0), (2, 0, 0), (1, 0, 0)]),
         calc_kwargs=sloppy_convergence),
], indirect=True)
def test_construct_full(calc):
    atoms = calc.atoms

    ref_C_nM = calc.wfs.collect_array('C_nM', 0, 0)
    ref_S_MM = calc.wfs.collect_array('S_MM', 0, 0)

    sym = LCAOSymmetry(calc)
    sym.construct_full_from_shells()

    test_C_nM = sym.C_nM
    test_S_MM = sym.S_MM

    assert test_C_nM.shape == ref_C_nM.shape
    assert np.allclose(test_C_nM, ref_C_nM)

    assert test_S_MM.shape == ref_S_MM.shape
    assert np.allclose(test_S_MM, ref_S_MM)

    assert np.allclose(test_C_nM @ test_S_MM @ test_C_nM.T, np.eye(len(test_C_nM))), 'should be eye!'

    test_P_ao = sym.P_ao

    # Center atom
    dist_a = np.linalg.norm(atoms.get_positions() - atoms.get_center_of_mass(), axis=1)
    C = np.argmin(dist_a)

    # Center atom should map to itself under all symmetry operations
    assert np.all(test_P_ao[C, :] == C)

    test_U_sixy = sym.U_sixy
    test_U_iMY = sym.U_iMY

    assert np.sum([U_MY.shape[1] for U_MY in test_U_iMY]) == test_U_iMY[0].shape[0]
    assert all([np.sum([U_xy.shape[1] for U_xy in test_U_ixy]) == test_U_ixy[0].shape[0] for test_U_ixy in test_U_sixy])
    test_proj_iMM = sym.P_iMM

    if True:  # Useful print
        with np.printoptions(linewidth=200, precision=2, suppress=True):
            print(np.concatenate(test_U_iMY, axis=1))
            for test_U_ixy in test_U_sixy:
                print(np.concatenate(test_U_ixy, axis=1))

    def norm(Q_MY):
        """ Stupid algorithm that makes sure column order should be consistent """
        sign_Y = np.sign(Q_MY[0])
        Q_MY = Q_MY.copy()
        Q_MY *= sign_Y[np.newaxis, :]
        order_Y = np.argsort(np.sum(Q_MY * np.arange(len(Q_MY))[:, np.newaxis], axis=0))
        return Q_MY[:, order_Y]

    for test_U_MY, test_proj_MM in zip(test_U_iMY, test_proj_iMM):
        eps_Y, psi_MY = np.linalg.eig(test_proj_MM)
        idx, = np.where(abs(eps_Y) > 1e-4)
        q, r = np.linalg.qr(psi_MY[:, idx])
        assert q.size == 0 or np.max(np.abs(q.imag)) < 1e-10
        q = q.real
        assert test_U_MY.shape == q.shape
        assert np.allclose(norm(test_U_MY), norm(q))


@pytest.mark.parametrize('calc', [
    dict(atoms=Atoms('H3', positions=[(0, 0, 0), (1, 0, 0), (2, 0, 0)]),
         calc_kwargs=sloppy_convergence),
], indirect=True)
def test_permuted_atoms(calc):
    def silent(*args, **kwargs):
        pass

    # Test object where atoms are not ordered in shell order
    params = calc.parameters
    atoms_test = calc.atoms

    test_sym = LCAOSymmetry(calc)
    test_sym.construct_full_from_shells()
    test_sym.analyze_ground_state_symmetry(log=silent)

    # Create reference where atoms are in shell order
    atoms_ref = atoms_test.copy()
    atoms_ref = atoms_ref[np.concatenate(test_sym.atomindex_sb, axis=0)]
    calc_ref = GPAW(**params)
    atoms_ref.calc = calc_ref
    atoms_ref.get_potential_energy()

    ref_sym = LCAOSymmetry(calc_ref)
    ref_sym.construct_full_from_shells()
    ref_sym.analyze_ground_state_symmetry(log=silent)

    assert np.allclose(calc.get_eigenvalues(), calc_ref.get_eigenvalues())

    # Calculate spectra
    test_tddft3 = LRTDDFT3(test_sym, verbose=False)
    out = test_tddft3.calculate_fast(verbose=False, progress=False)
    test_dE_N, test_K_NN, _ = out['delta_N'], out['K_NN'], out['mu_Nv']
    test_vals_n, _ = test_tddft3.casida(test_dE_N, test_K_NN)

    ref_tddft3 = LRTDDFT3(ref_sym, verbose=False)
    out = ref_tddft3.calculate_fast(verbose=False, progress=False)
    ref_dE_N, ref_K_NN, _ = out['delta_N'], out['K_NN'], out['mu_Nv']
    ref_vals_n, _ = ref_tddft3.casida(ref_dE_N, ref_K_NN)

    assert np.allclose(ref_dE_N, test_dE_N)
    assert np.allclose(test_vals_n, ref_vals_n)


@pytest.mark.parametrize('calc', [
    dict(atoms=Atoms('Al3', positions=[(0, 0, 0), (1, 0, 0), (2, 0, 0)]),
         calc_kwargs=sloppy_convergence),
], indirect=True)
def test_projectors_general_sptype(calc):
    atoms = calc.atoms
    calc.set_positions(atoms)

    symmetry = detect_symmetry(atoms)

    atomindex_sb = symmetry.get_atomic_shells(atoms)
    P_sbo = symmetry.get_symmetry_permutations_by_shell(atoms, atomindex_sb)

    l_sj = [[phit.get_angular_momentum_number() for phit in calc.setups[atomindex_b[0]].phit_j]
            for atomindex_b in atomindex_sb]
    l_sj = np.array(l_sj)

    # Test that general gives same as sptype
    assert np.array_equal(l_sj[0], [0, 1]), 'This unit test tests against sp operations'
    for s in range(2):
        ref_U_iMY, ref_P_iMM = symmetry_projectors_sptype(symmetry, P_sbo[s])
        test_U_iMY, test_P_iMM = symmetry.symmetry_projectors_general(l_sj[s], P_sbo[s])
        for i in range(symmetry.Ni()):
            assert np.allclose(test_U_iMY[i], ref_U_iMY[i])
            assert np.allclose(test_P_iMM[i], ref_P_iMM[i])

        for o in range(symmetry.No()):
            ref_P_XX = symmetry_operator_sptype(symmetry, P_sbo[s], o)
            test_P_XX = symmetry.symmetry_operator_general(l_sj[s], P_sbo[s], o)
            assert np.allclose(test_P_XX, ref_P_XX)


@pytest.mark.parametrize('calc', [
    dict(atoms=Atoms('H2Na', positions=[(0, 0, 0), (2, 0, 0), (1, 0, 0)]),
         calc_kwargs=sloppy_convergence),
    dict(atoms=Atoms('Na3', positions=[(0, 0, 0), (1, 0, 0), (2, 0, 0)]),
         calc_kwargs=sloppy_convergence),
    dict(atoms=Atoms('Na3', positions=[(0, 0, 0), (2, 0, 0), (1, 0, 0)]),
         calc_kwargs=sloppy_convergence),
    'Al13.xyz.gpw',
], indirect=True)
def test_shuffle_K_MMMM(calc):
    from runAlRIK import calculate_K_MMMM

    np.set_printoptions(linewidth=200, precision=3, suppress=True)
    atoms = calc.atoms

    sym = LCAOSymmetry(calc)
    sym.construct_full_from_shells()

    atomsorder_A = np.concatenate(sym.atomindex_sb, axis=0)

    shuffled_atoms = atoms[atomsorder_A]
    _, ref_K_MMMM = calculate_K_MMMM(shuffled_atoms)
    _, unshuffled_K_MMMM = calculate_K_MMMM(atoms)
    test_K_MMMM = sym.tensor_shuffler(unshuffled_K_MMMM, [0, 1, 2, 3])

    assert test_K_MMMM.shape == ref_K_MMMM.shape
    assert np.allclose(test_K_MMMM, ref_K_MMMM)


@pytest.mark.parametrize('calc', [
    dict(atoms=Atoms('H2Na', positions=[(0, 0, 0), (2, 0, 0), (1, 0, 0)]),
         calc_kwargs=sloppy_convergence),
    dict(atoms=Atoms('Na3', positions=[(0, 0, 0), (1, 0, 0), (2, 0, 0)]),
         calc_kwargs=sloppy_convergence),
    dict(atoms=Atoms('Na3', positions=[(0, 0, 0), (2, 0, 0), (1, 0, 0)]),
         calc_kwargs=sloppy_convergence),
    'Al13.xyz.gpw',
], indirect=True)
def test_shells_classification(calc):
    atoms = calc.atoms

    atoms_rep = AtomsRepresentation.from_atoms(atoms)
    shells = Shells.from_atoms_representations(atoms_rep)
    for shell in shells:
        dist_b = np.linalg.norm(shell.R_bc, axis=1)
        assert np.allclose(dist_b, dist_b[0])


@pytest.mark.parametrize('calc', [
    dict(atoms=Atoms('H2Na', positions=[(0, 0, 0), (2, 0, 0), (1, 0, 0)]),
         calc_kwargs=sloppy_convergence),
    dict(atoms=Atoms('Na3', positions=[(0, 0, 0), (1, 0, 0), (2, 0, 0)]),
         calc_kwargs=sloppy_convergence),
    dict(atoms=Atoms('Na3', positions=[(0, 0, 0), (2, 0, 0), (1, 0, 0)]),
         calc_kwargs=sloppy_convergence),
    'Al13.xyz.gpw',
], indirect=True)
def test_analyze_ground_state_symmetry(calc):
    np.set_printoptions(linewidth=200, precision=3, suppress=True)

    sym = LCAOSymmetry(calc)
    sym.construct_full_from_shells()
    sym.analyze_ground_state_symmetry(log=do_nothing)

    test_C_diMn = sym.C_diMn  # Created by analyze_ground_state_symmetry

    def test_analyze_ground_state_symmetry(eps_n, f_n, C_nM, S_MM, U_iMY, n_dl):
        import scipy

        # degenerate group, irrep index, actual basis index, symmetry projected band index
        # Loop over degenerate eigenvalue groups
        for d, n_l in enumerate(n_dl):
            # Here, some rounding might take place, since tolerance was used here, but here all the
            # eigenvalues are assumed the same
            for irrep in range(len(U_iMY)):
                U_Mx = U_iMY[irrep]
                if U_Mx.shape[1] == 0:
                    continue
                P_MM = np.dot(U_Mx, U_Mx.T)
                rho_MM = np.dot(C_nM[n_l].T, C_nM[n_l])
                U_MM = np.dot(P_MM.T, np.dot(S_MM, np.dot(np.dot(rho_MM, S_MM), P_MM)))
                values_n, C_Mn = scipy.linalg.eigh(U_MM, S_MM)

                zerothresh = 1e-4
                C_Mn = C_Mn[:, values_n > zerothresh]
                if C_Mn.shape[1] == 0:
                    continue

                # assert np.allclose(nonzero_n, nonzero1_n)

                test_C_Mn = test_C_diMn[d][irrep]
                test_P_MM = S_MM @ test_C_Mn @ test_C_Mn.T.conj()
                ref_P_MM = S_MM @ C_Mn @ C_Mn.T.conj()
                assert np.allclose(test_P_MM, ref_P_MM)
                assert np.allclose(test_P_MM @ test_P_MM, test_P_MM)

    test_analyze_ground_state_symmetry(sym.eps_n, sym.f_n, sym.C_nM, sym.S_MM, sym.U_iMY, sym.n_dl)
