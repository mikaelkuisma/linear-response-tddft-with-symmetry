from gpaw import GPAW
from sys import argv
from gpaw.mpi import world, size, rank
from symmetry import Symmetry, detect_symmetry
from ase import Atoms
from gpaw.external import ConstantElectricField
from gpaw.lrtddft2.lr_communicators import LrCommunicators

import numpy as np
import scipy
from gpaw.utilities import pack, pack2
from gpaw.lfc import LocalizedFunctionsCollection as LFC, BasisFunctions

from gpaw.lrtddft2 import LrTDDFT2

if world.size == 40:
    lr_comms = LrCommunicators(world, 4, 10)
else:
    lr_comms = LrCommunicators(world, 1, 1)

calc = GPAW(argv[1]+".gpw", communicator=lr_comms.dd_comm)

lr = LrTDDFT2(argv[1]+"_lr",
               calc,
               fxc="LDA",
               max_energy_diff=1e9,
               lr_communicators=lr_comms)
lr.K_matrix.fxc_pre = 0.0  # Ignore fxc part

w, S, R, Sx, Sy, Sz = lr.get_transitions(units='au')
print(w, S)

(w, S, R, Sx, Sy, Sz) = lr.get_spectrum(energy_step=0.0025, width=0.05)
f = open('reference.spec', 'w')
for i in range(len(w)):
    print("%.8f %.8f" % (w[i], S[i]), file=f)
f.close()

