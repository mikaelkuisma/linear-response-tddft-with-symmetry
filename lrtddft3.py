from __future__ import annotations
from gpaw import GPAW
from sys import argv
# 1. Create a basic ground state LCAO calculator

from symmetry import AtomsRepresentation, Shells, LCAOShells, PointGroupBaseClass
import numpy as np
import scipy
from gpaw.utilities import pack, pack2

# GPAW --> analyse symmetry of KS states --> LRTDDFT --> use symmetry to do orbital products
# s is atom shell index
# d is a degenerate subspace index
# l is index within a degenerate subspace
# In case of accidental degeneracies, group eigenvalues by their energy
# i is irreducible representation index
# g is conjugacy class index
# o is symmetry operation index
# i is occupied electron index
# a is atom index
# b is local atom index on a given shell
# a is unoccupied electron index
# M is basis function index
# m is local basis function index on a given shell
# p is permutation index


def setup_progressbar(progress=True):
    import contextlib

    def do_nothing(*args, **kwargs):
        pass

    @contextlib.contextmanager
    def dummy_bar(*args, **kwargs):
        yield do_nothing

    if not progress:
        # Dummy do nothing
        return dummy_bar
    try:
        from alive_progress import alive_bar
    except ImportError:
        print('pip install alive-progress for progress bar!')
        return dummy_bar

    return alive_bar


class LCAOSymmetry:
    def __init__(self, calc: GPAW):
        """ LCAO point group symmetry object

        1) Detects symmetry of atoms
        2) Divides the atoms into shells around the center of mass
           (atoms in one shell have same distance and are of same species)
        3) Within each shell:
          a) Find the mapping between atoms in the shell under each symmetry operation
          b) Find the mapping of basis functions in the shell under each symmetry operation
             For example a px basis function of atom 1 may map to pz of atom 2 under some operation
          c) Extract the wavefunction coefficients, projectors, and overlap matrix of the full system
             corresponding to the shell

        """
        assert calc.parameters.mode == 'lcao'
        atoms = calc.atoms
        atoms_rep = AtomsRepresentation.from_atoms(atoms)
        self.atoms_rep = atoms_rep
        print('Detected symmetry', atoms_rep.point_group)
        shells = Shells.from_atoms_representations(atoms_rep)
        shells = LCAOShells.from_shells(shells, calc.setups)
        self.shells = shells

        self.calc = calc
        calc.set_positions()

        self.P_sbo = shells.P_sbo
        # self.l_j = np.array(shells.l_sj[0])

        # Get eigenvalues and occupations
        wf = calc.wfs.kpt_u[0]
        assert wf.q == 0
        self.eps_n = wf.eps_n
        self.f_n = wf.f_n
        M_a = calc.wfs.setups.M_a + [calc.wfs.setups.nao]
        self.M_a = M_a

        # Before C_ixM <- U_ixM @ C_nM

        # Now
        # U_isxm <- U_ixM
        # C_nsm <- C_nM  # Split into shells
        # C_nisx <- U_isxm @ C_nsm  # Split into irreps
        self.n_dl = self.group_eigenvalues(self.eps_n)

        # Extract overlaps and coefficients corresponding to shells
        self.x_sb, self.C_snx, self.S_sxM = shells.get_coefficients_overlaps_by_shell(
            wf.C_nM, wf.S_MM)

    @property
    def point_group(self) -> PointGroupBaseClass:
        return self.atoms_rep.point_group

    def construct_full_from_shells(self):
        """ Constructs matrices for the full system from matrices for the subshells

        The resulting matrices must be consistent with the self.atoms object. Therefore
        some permutations is needed
        """

        Nn = len(self.C_snx[0])  # Number of bands
        NM = np.sum([x_b[-1] for x_b in self.x_sb])  # Number of global basis indices
        Na = np.sum([len(shell.a_b) for shell in self.shells.shells])  # Number of global atoms
        C_nM = np.zeros((Nn, NM))
        S_MM = np.zeros((NM, NM))

        for s, shell in enumerate(self.shells.shells):
            x_b = self.x_sb[s]
            S_xM = self.S_sxM[s]
            C_nx = self.C_snx[s]
            for b, (xstart, xstop) in enumerate(zip(x_b[:-1], x_b[1:])):
                a = shell.a_b[b]  # Global atom index
                Mstart, Mstop = self.M_a[a], self.M_a[a + 1]
                local_x_slice = slice(xstart, xstop)
                global_M_slice = slice(Mstart, Mstop)
                C_nM[:, global_M_slice] = C_nx[:, local_x_slice]
                S_MM[global_M_slice, :] = S_xM[local_x_slice, :]

        self.C_nM = C_nM
        self.S_MM = S_MM

        self.P_ao = np.zeros((Na, self.P_sbo[0].shape[1]), dtype=int)

        # Loop over every shell
        for s, shell in enumerate(self.shells.shells):
            atomindex_b = shell.a_b
            P_bo = self.P_sbo[s]
            # Loop over atoms in shell
            for localb, globala in enumerate(atomindex_b):
                # local atom localb maps to local P_bo[localb, o] under operation o
                # -> global atom globala maps to global atomindex_b[P_bo[localb, o]]
                self.P_ao[globala, :] = atomindex_b[P_bo[localb, :]]

        self.U_iMY = []
        self.P_iMM = []

        def norm(Q_MY):
            """ Stupid algorithm that makes sure column order should be consistent """
            sign_Y = np.sign(Q_MY[0])
            Q_MY = Q_MY.copy()
            Q_MY *= sign_Y[np.newaxis, :]
            order_Y = np.argsort(np.sum(Q_MY * np.arange(len(Q_MY))[:, np.newaxis], axis=0))
            return Q_MY[:, order_Y]

        # Loop over irreps
        for i in range(self.point_group.Ni()):
            Ystart = 0
            # Number of eigenvalues in each shell for this irrep
            Ny_s = [U_ixy[i].shape[1] for U_ixy in self.shells.U_sixy]
            # Total number of eigenvalues for this irrep
            NY = np.sum(Ny_s)

            U_MY = np.zeros((NM, NY))
            for s, shell in enumerate(self.shells.shells):
                x_b = self.x_sb[s]
                atomindex_b = shell.a_b
                Ny = Ny_s[s]
                U_xy = self.shells.U_sixy[s][i]
                # P_xx = self.P_sixx[s][i]  # Not used
                for b, (xstart, xstop) in enumerate(zip(x_b[:-1], x_b[1:])):
                    a = atomindex_b[b]  # Global atom index
                    Mstart, Mstop = self.M_a[a], self.M_a[a + 1]
                    local_x_slice = slice(xstart, xstop)
                    global_M_slice = slice(Mstart, Mstop)
                    global_Y_slice = slice(Ystart, Ystart + Ny)
                    U_MY[global_M_slice, global_Y_slice] = U_xy[local_x_slice, :]
                Ystart += Ny
            self.U_iMY.append(U_MY)
            P_MM = np.dot(U_MY, U_MY.T)
            self.P_iMM.append(P_MM)

            assert Ystart == NY

    def permute_M_a(self):
        # capital stands for new
        atomsorder_A = np.concatenate(self.atomindex_sb, axis=0)

        dM_a = np.diff(self.M_a)
        newM_A = np.zeros_like(self.M_a[:-1])
        newM_A[:] = dM_a[atomsorder_A]
        return np.array([0] + list(np.cumsum(newM_A)))

    def row_shuffler(self, A_Mx):
        atomsorder_A = np.concatenate(self.atomindex_sb, axis=0)

        new_A_Mx = np.zeros_like(A_Mx)
        newM_A = self.permute_M_a()
        for newa, olda in enumerate(atomsorder_A):
            oldslice = slice(self.M_a[olda], self.M_a[olda + 1])
            newslice = slice(newM_A[newa], newM_A[newa + 1])
            new_A_Mx[newslice] = A_Mx[oldslice]

        return new_A_Mx

    def tensor_shuffler(self, A, axes):
        new_A = A
        for ax in axes:
            new_A = self.row_shuffler(new_A.swapaxes(0, ax)).swapaxes(0, ax)
        return new_A

    def group_eigenvalues(self, eps_n, tol=1e-4):
        chunks_dl = []
        n_l = []
        chunks_dl.append(n_l)
        for n in range(len(eps_n)):
            if len(n_l) == 0:
                n_l.append(n)
            else:
                E = eps_n[n_l[0]]
                if abs(E - eps_n[n]) > tol:  # New subspace
                    # New subspace
                    n_l = []
                    chunks_dl.append(n_l)
                n_l.append(n)
        return chunks_dl

    def analyze_ground_state_symmetry(self, log=print):
        eps_n, f_n, C_nM, S_MM = self.eps_n, self.f_n, self.C_nM, self.S_MM
        # P_sbo = self.P_sbo
        # degenerate group, irrep index, actual basis index, symmetry projected band index
        self.C_diMn = C_diMn = []
        self.eps_d = eps_d = []
        self.f_d = f_d = []
        # Loop over degenerate eigenvalue groups
        for d, n_l in enumerate(self.n_dl):
            # Here, some rounding might take place, since tolerance was used here, but here all the
            # eigenvalues are assumed the same
            eps_d.append(eps_n[n_l[0]])
            f_d.append(f_n[n_l[0]])
            log('eig: %d x %.5f' % (len(n_l), eps_n[n_l[0]]))

            C_iMn = self.analyze_ground_state_symmetry_by_shell(C_nM[n_l], S_MM, f_d[d], log=log)
            C_diMn.append(C_iMn)

    def analyze_ground_state_symmetry_by_shell(self,
                                               C_nM,
                                               S_MM,
                                               occ,
                                               log=print):
        """
        Parameters
        ----------
        C_nM
            Sliced LCAO coefficient
            Note that here, n is a sliced index corresponding to the shell
        """
        U_iMY = self.U_iMY
        C_iMn = []
        for irrep in range(len(U_iMY)):
            U_Mx = U_iMY[irrep]
            if U_Mx.shape[1] == 0:
                C_iMn.append(np.array([[]]))
                continue
            USC_xn = U_Mx.T @ S_MM @ C_nM.T
            U_xx = USC_xn @ USC_xn.T
            S_xx = U_Mx.T @ S_MM @ U_Mx
            Shalfinv_xx = np.linalg.inv(scipy.linalg.sqrtm(S_xx))
            # eigh guarantees orthonormality
            values_n, Cprime_xn = scipy.linalg.eigh(Shalfinv_xx @ U_xx @ Shalfinv_xx)
            C_xn = Shalfinv_xx @ Cprime_xn

            zerothresh = 1e-4
            C_iMn.append(U_Mx @ C_xn[:, values_n > zerothresh])
            first = True
            for n, occ in enumerate(values_n):
                if abs(occ) > 1e-4:
                    if first:
                        log(" %.2f  %5s : " % (occ, self.point_group.names_i[irrep]), end='')
                        first = False
                    log("  %.5f" % (values_n[n]), end='')
            if not first:
                log()
        return C_iMn


class LRTDDFT3:
    def __init__(self,
                 symmetry: LCAOSymmetry,
                 irreps: None | list[int] = None,
                 verbose: bool = False):
        if irreps is None:
            irreps = range(symmetry.symmetry.Ni())
        self.l_j = symmetry.l_j
        self.irreps = irreps
        self.calc = symmetry.calc  # not pretty
        order = symmetry.symmetry.order
        S_MM = symmetry.S_MM
        # We loop over all of the occupied and unoccupied orbitals
        eps_d, n_dl, U_iMY, C_diMn, f_d = symmetry.eps_d, symmetry.n_dl, symmetry.U_iMY, symmetry.C_diMn, symmetry.f_d
        self.n_dl = n_dl

        # This is needed to shuffle the K_MMMM matrix
        def shuffle_MMMM(a_MMMM):
            return symmetry.tensor_shuffler(a_MMMM, [0, 1, 2, 3])

        self.shuffle_MMMM = shuffle_MMMM

        # N is ehspace index
        self.ehspace_N = ehspace_N = []  # N = (i, a)
        self.dE_N = dE_N = []  # Kohn-Sham eigenvalue difference epsilon_(a_N) - epsilon_(i_N)

        if verbose:
            def log(*args, **kwargs):
                print(*args, **kwargs)
        else:
            def log(*args, **kwargs):
                pass

        def fmt(n_l, f, eps, C_Mn, symname):
            return f'{repr(n_l)} {f:.2f} {eps:.5f} ({C_Mn.shape[1]:.0f}{symname})'

        for occd in range(len(eps_d)):
            occn_l = n_dl[occd]
            if f_d[occd] < 1e-4:
                continue
            for unoccd in range(len(eps_d)):
                if f_d[unoccd] > 1.999:
                    continue

                unoccn_l = n_dl[unoccd]
                for occi in range(len(U_iMY)):
                    occC_Mn = C_diMn[occd][occi]
                    if occC_Mn.shape[1] == 0:
                        continue
                    for unocci in range(len(U_iMY)):
                        unoccC_Mn = C_diMn[unoccd][unocci]
                        if unoccC_Mn.shape[1] == 0:
                            continue
                        occstr = fmt(occn_l, f_d[occd], eps_d[occd], occC_Mn, symmetry.symmetry.names_i[occi])
                        unoccstr = fmt(unoccn_l, f_d[unoccd], eps_d[unoccd], unoccC_Mn, symmetry.symmetry.names_i[unocci])
                        log(f'Transition {occstr} -> {unoccstr}')
                        character_ig = np.array(symmetry.symmetry.character_ig)
                        product_g = character_ig[occi, :] * character_ig[unocci, :]
                        sizes_g = np.array(list(map(len, symmetry.symmetry.ops_g)))
                        log("%3s x %3s = " % (symmetry.symmetry.names_i[occi], symmetry.symmetry.names_i[unocci]), end='')
                        for target_irrep in range(len(U_iMY)):
                            proj = np.sum(product_g * sizes_g * character_ig[target_irrep, :].T) / order
                            if np.abs(proj) > 1e-4:
                                if target_irrep in irreps:
                                    log(" {", end='')
                                log(" %3s (%.5f) " % (symmetry.symmetry.names_i[target_irrep], proj), end='')
                                if target_irrep in irreps:
                                    log("} ", end='')

                        log()
                        for target_irrep in irreps:
                            redocc_onn = []
                            redunocc_onn = []
                            rho_nnnn = 0.0
                            sizes_g = np.array(list(map(len, symmetry.symmetry.ops_g)))

                            for o in range(symmetry.symmetry.No()):
                                P_MM = symmetry.symmetry.symmetry_operator_general(self.l_j, symmetry.P_ao, o)
                                redocc_nn = np.dot(occC_Mn.T, np.dot(S_MM, np.dot(P_MM, occC_Mn)))
                                redocc_onn.append(redocc_nn)
                                redunocc_nn = np.dot(unoccC_Mn.T, np.dot(S_MM, np.dot(P_MM, unoccC_Mn)))
                                redunocc_onn.append(redunocc_nn)
                                chi = symmetry.symmetry.character_ig[target_irrep, symmetry.symmetry.g_o[o]]
                                drho_nnnn = np.kron(redocc_nn, redunocc_nn) * chi / order * symmetry.symmetry.character_ig[target_irrep, -1]  # XXX Hard coded -1
                                # Note: drho_nnnn is not required to be Hermitian. But after the loop rho_nnnn is
                                rho_nnnn += drho_nnnn

                            rhoerr_nnnn = rho_nnnn - rho_nnnn.T
                            with np.printoptions(linewidth=200, precision=3):
                                assert np.linalg.norm(rhoerr_nnnn) < 1e-8, rhoerr_nnnn
                            vals, vectors_nnk = np.linalg.eigh(rho_nnnn)
                            tot = np.trace(rho_nnnn)
                            if abs(tot) > 1e-4:
                                log(" %s (%.5f) " % (symmetry.symmetry.names_i[target_irrep], tot), end='')
                                log(" eigs: ", end='')
                                for k, val in enumerate(vals):
                                    if abs(val) > 1e-4:
                                        log("%05d=%.3f " % (len(ehspace_N), val), end='')
                                        ehspace_N.append((occn_l, unoccn_l, eps_d[occd], eps_d[unoccd],
                                                          f_d[occd], f_d[unoccd], occC_Mn, unoccC_Mn, vectors_nnk[:, k]))
                                        dE_N.append(eps_d[unoccd] - eps_d[occd])

                                        log()

    def charge_density(self, occC_Mn, unoccC_Mn, vec_nn, do_pack2=False):
        calc = self.calc
        rho_MM = np.dot(occC_Mn, np.dot(np.reshape(vec_nn, (occC_Mn.shape[1], unoccC_Mn.shape[1])), unoccC_Mn.T))
        q = 0
        # print("Norm of symmetry adapted rho_MM (should be 0)", np.trace(np.dot(S_MM, rho_MM)))
        rho_MM = (rho_MM + rho_MM.T) / 2
        rho_MM = rho_MM.real.copy()
        Q_aL = {}
        D_ap = {}
        for a in range(len(calc.atoms)):
            P_Mi = calc.wfs.P_aqMi[a][q]
            D_ii = np.dot(P_Mi.T, np.dot(rho_MM, P_Mi))
            if do_pack2:
                D_p = pack2(D_ii)
            else:
                D_p = pack(D_ii)
            D_ap[a] = D_p
            Q_aL[a] = np.dot(D_p, calc.density.setups[a].Delta_pL).real.copy()
        rho_G = calc.density.gd.zeros()
        calc.wfs.basis_functions.construct_density(rho_MM, rho_G, q)
        rho_g = calc.density.finegd.zeros()
        calc.density.interpolator.apply(rho_G, rho_g)
        calc.density.ghat.add(rho_g, Q_aL)
        return rho_g, D_ap

    def sort_N(self, dE_max=np.inf):
        """ Sort the electron-hole space according to eigenvalue differences

        Parameters
        ----------
        dE_max
            Energy cutoff for electron hole differences in eV

        Return
        ------
        Sorted electron-hole space, up to cutoff dE_max
        """
        from ase.units import Hartree

        N_N = np.argsort(self.dE_N)
        sort_dE_N = np.array(self.dE_N)[N_N] / Hartree
        ordehspace_N = []
        for N in N_N[sort_dE_N < dE_max]:
            ordehspace_N.append(self.ehspace_N[N])

        return ordehspace_N

    def calculate(self, dE_max=np.inf, verbose=False):
        """ WARNING: This doesn't work if the wavefunctions have been permuted into shells """
        ordehspace_N = self.sort_N(dE_max)

        calc = self.calc

        nN = len(ordehspace_N)
        delta_N = np.zeros(nN)  # Eigenvalue differences
        K_NN = np.zeros((nN, nN))
        mu_Nv = np.zeros((nN, 3))
        N = 0  # Counter for the purpose of visualizing progress
        maxN = nN**2 // 2
        for N1, (occ1_l, unocc1_l, occeps1, unocceps1, occf1, unoccf1, occC1_Mn, unoccC1_Mn, vec1_nn) in enumerate(ordehspace_N):
            rho1_g, D1_ap = self.charge_density(occC1_Mn, unoccC1_Mn, vec1_nn)
            VHt1_g = calc.density.finegd.zeros()
            calc.hamiltonian.poisson.solve(VHt1_g, rho1_g, charge=None)

            mu = calc.density.finegd.calculate_dipole_moment(rho1_g, center=True)
            mu_Nv[N1, :] = mu
            delta_N[N1] = unocceps1 - occeps1

            H_ap = {}
            for a, D1_p in D1_ap.items():
                C_pp = calc.wfs.setups[a].M_pp
                H_ap[a] = 2.0 * np.dot(C_pp, D1_p)

            for N2, (occ2_l, unocc2_l, occeps2, unocceps2, occf2, unoccf2, occC2_Mn, unoccC2_Mn, vec2_nn) in enumerate(ordehspace_N[:N1 + 1]):
                rho2_g, D2_ap = self.charge_density(occC2_Mn, unoccC2_Mn, vec2_nn)
                K = calc.density.finegd.integrate(VHt1_g * rho2_g)
                for a, D2_p in D2_ap.items():
                    # print("Corr", np.dot(D2_p, H_ap[a]))
                    K = K + np.dot(D2_p, H_ap[a])

                K_NN[N1, N2] = K
                if verbose:
                    print(f'[{N:05.0f}/{maxN:05.0f}]  [{occ1_l} -> [{unocc1_l}]] V [{occ2_l}] -> [{unocc2_l}]: K {K}')
                N = N + 1

        ret = dict(delta_N=delta_N, mu_Nv=mu_Nv, K_NN=K_NN)

        return ret

    def calculate_fast(self, dE_max=np.inf, verbose=False, progress=False):
        from runAlRIK import calculate_K_MMMM
        ordehspace_N = self.sort_N(dE_max)

        calc = self.calc
        _, K_MMMM = calculate_K_MMMM(self.calc.atoms)
        # K_MMMM = self.shuffle_MMMM(K_MMMM)  # We should not shuffle the K_MMMM!!

        nN = len(ordehspace_N)
        delta_N = np.zeros(nN)  # Eigenvalue differences
        K_NN = np.zeros((nN, nN))
        mu_Nv = np.zeros((nN, 3))
        N = 0  # Counter for the purpose of visualizing progress
        maxN = nN * (nN + 1) // 2

        # Progressbar
        progressbar = setup_progressbar(progress)
        with progressbar(maxN) as bar:
            for N1, (occ1_l, unocc1_l, occeps1, unocceps1, occf1, unoccf1, occC1_Mn, unoccC1_Mn, vec1_nn) in enumerate(ordehspace_N):
                rho1_g, _ = self.charge_density(occC1_Mn, unoccC1_Mn, vec1_nn)
                vec1_nn = np.reshape(vec1_nn, (occC1_Mn.shape[1], unoccC1_Mn.shape[1]))
                mu = calc.density.finegd.calculate_dipole_moment(rho1_g, center=True)
                mu_Nv[N1, :] = mu
                delta_N[N1] = unocceps1 - occeps1
                K_OP = np.einsum('MNOP,Mi,Nj,ij', K_MMMM, occC1_Mn, unoccC1_Mn, vec1_nn, optimize=True)

                for N2, (occ2_l, unocc2_l, occeps2, unocceps2, occf2, unoccf2, occC2_Mn, unoccC2_Mn, vec2_nn) in enumerate(ordehspace_N[:N1 + 1]):
                    vec2_nn = np.reshape(vec2_nn, (occC2_Mn.shape[1], unoccC2_Mn.shape[1]))
                    K = np.einsum('OP,Ok,Pl,kl', K_OP, occC2_Mn, unoccC2_Mn, vec2_nn, optimize=True)
                    # K = np.einsum('MNOP,Mi,Nj,Ok,Pl,ij,kl', K_MMMM, occC1_Mn, unoccC1_Mn, occC2_Mn, unoccC2_Mn, vec1_nn, vec2_nn, optimize=True)
                    assert np.abs(K.imag) < 1e-8
                    K = K.real

                    K_NN[N1, N2] = K
                    K_NN[N2, N1] = K
                    if verbose:
                        print(f'[{N:05.0f}/{maxN:05.0f}]  [{occ1_l} -> [{unocc1_l}]] V [{occ2_l}] -> [{unocc2_l}]: K {K}')
                    bar()
                    N = N + 1

        ret = dict(delta_N=delta_N, mu_Nv=mu_Nv, K_NN=K_NN)

        return ret

    def casida(self, dE_N, K_NN):
        """ Solve the Casida equation

        The Casida equation is (for an adiabatic kernel and real wave functions)
        the eigenvalue equation for ω² (vals_n) and Z (vectors_Nn)

         (Δ²  + 4 √Δ K √Δ) Z = ω² Z

        Where K is the Coulomb matrix

        K    = ∫ dr dr' n(r) |r-r'|⁻¹  n(r')
         NN'             N            N'

        and Δ the matrix of Kohn-Sham eigenvalue differences

        Δ   = δ    ω
         NN    NN'  N

        N corresponds to occupied-unoccupied pair (i, a) so that

        ω  = ε - ε .
         N    a   i
        """

        sqrtD_NN = np.diag(np.sqrt(dE_N))

        C = sqrtD_NN**4 + 4 * sqrtD_NN @ K_NN @ sqrtD_NN

        vals_n, vectors_Nn = np.linalg.eigh(C)

        return vals_n, vectors_Nn

    def spectrum(self,
                 vals_n,
                 vectors_Nn,
                 dE_N,
                 mu_Nv,
                 min_energy=0.0,
                 max_energy=30.0,
                 energy_step=0.0025,
                 width=0.2):
        """ Construct a spectrum from the Casida eigenvalues and eigenvectors

                          ω
                           N
        ɑ(ω) = Σ  2/3  -------  |μ |²
               N       ω² - ω²    N
                        N

        S(ω) = 2ω / π * Im[ɑ(ω)]

        """
        from ase.units import Hartree

        freq_w = np.arange(min_energy, max_energy, energy_step)
        abs_w = np.zeros_like(freq_w)
        for (n, omega2) in enumerate(vals_n):
            if omega2 < 0:
                print(f'WARNING negative eigenvalue {n}: {omega2}. Skipping')
                continue
            omega = omega2**0.5
            mu_c = (vectors_Nn[:, n] * (2.0 * dE_N / omega)**0.5) @ mu_Nv
            osc = 2.0 / 3.0 * omega * np.sum(mu_c**2)
            c = osc / width / np.sqrt(2 * np.pi)
            abs_w += c * np.exp((-.5 / width / width) * np.power(freq_w - omega * Hartree, 2))
        return freq_w, abs_w

    def plot_spectrum(self, freq_w, abs_w, ref_freq_w=None, ref_abs_w=None, figname=None):
        from matplotlib import pyplot as plt

        fig, ax = plt.subplots(1, 1)

        ax.plot(freq_w, abs_w, color='cornflowerblue')
        ax.set_xlabel('Energy (eV)')
        ax.set_ylabel('Dipole strength function (1/eV)')

        print(np.trapz(abs_w, freq_w))

        if ref_freq_w is not None:
            ax.plot(ref_freq_w, ref_abs_w, 'r--')

        if figname is None:
            plt.show()
        else:
            fig.savefig(figname)


if __name__ == "__main__":
    calc = GPAW(argv[1], txt='/dev/null')
    sym = LCAOSymmetry(calc)
    sym.construct_full_from_shells()
    sym.analyze_ground_state_symmetry()
    from sys import exit
    exit(0)
    tddft3 = LRTDDFT3(sym, irreps=[9], verbose=True)
    out = tddft3.calculate_fast(verbose=False, progress=True)
    dE_N, K_NN, mu_Nv = out['delta_N'], out['K_NN'], out['mu_Nv']
    vals_n, vectors_Nn = tddft3.casida(dE_N, K_NN)

    spec = np.loadtxt('Al13.xyz.spec.dat')
    freq_w, abs_w = tddft3.spectrum(vals_n, vectors_Nn, dE_N, mu_Nv)
    tddft3.plot_spectrum(freq_w, abs_w, spec[:, 0], spec[:, 3])
