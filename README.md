(c) Mikael Kuisma Jakub Fojt
Symmetry adapted LCAO-LR-TDDFT with GPAW

Usage: 
 1. Calculate Al13.xyz.gpw by running runAl.py Al13.xyz
 2. Calculate reference TDDFT spectrum by running tddft_Al.py Al13.xyz
 3. Calculate symmetry adapted matrix elements by running symtddft_Al.py Al13.xyz
 4. Calculate symmetry adapted spectrum by running Al_casida.py Al13.xyz
 5. Plot results with gnuplot

 plot "reference.spec" u 1:2 title "Traditional LrTDDFT" w l lw 4, "sym.spec" u 1:($2*9) title "Symmetry adapted TDDFT" w l lw 2
  (Factor of 9 has been fixed in this version, but data is with x9.)

Hard coded (among others):
 * Oh group


## Symmetry decomposition

In LCAO we expand the wave funcitons $`\psi_n`$ of Kohn-Sham states $`n`$ in basis functions $`\phi_\mu`$

```math
\psi_n (\vec{r}) = \sum_\mu C_{n\mu} \phi_\mu(\vec{r})
```

If there are point group symmetries in the system, we can decompose the wave functions into irreducible representations (irreps).
For each of the $`N_i`$ irreps $`i`$ we will have a number of irreducible basis functions $`\phi^{(i)}_x(\vec{r})`$.

```math
\phi_x^{(i)}(\vec{r}) = \sum_\mu U_{ix\mu} \phi_\mu(\vec{r})
```

The indices $`i`$ and $`x`$ together span the same space as $`\mu`$. We can thus represent $`U_{ix\mu}`$ as a square matrix with rows indexed by $`ix`$ and columns by $`\mu`$.

The inverse of $`U`$ is used to transform the irreducible basis functions into the conventional LCAO basis functions

```math
\phi_\mu(\vec{r}) = \sum_{ix} \left[U^{-1}\right]_{ix\mu{} } \phi_x^{(i)}(\vec{r})
```

