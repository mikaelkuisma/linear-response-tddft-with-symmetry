from gpaw import GPAW
from sys import argv
# 1. Create a basic ground state LCAO calculator
from symmetry import Symmetry, detect_symmetry
from ase import Atoms
from gpaw.external import ConstantElectricField

import numpy as np
import scipy
from gpaw.utilities import pack, pack2
from gpaw.lfc import LocalizedFunctionsCollection as LFC, BasisFunctions

target_irreps = [ 9 ]  # DIPOLE
#print(Symmetry('Oh').names_i)
#target_irreps = range(10) # ALL irreps

# d is a degenerate subspace index
# l is index within a degenerate subspace
def group_eigenvalues(eps_n, tol=1e-4):
    chunks_dl = []
    n_l = []
    chunks_dl.append(n_l)
    for n in range(len(eps_n)):
        if len(n_l) == 0:
            n_l.append(n)
        else:
            E = eps_n[n_l[0]]
            if abs(E - eps_n[n])>tol: # New subspace
                # New subspace
                n_l = []
                chunks_dl.append(n_l)
            n_l.append(n)
    return chunks_dl

calc = GPAW(argv[1]+".gpw")
calc.set_positions()
atoms = calc.atoms
symmetry = detect_symmetry(atoms)

N = len(atoms)
charge = 0
if N == 79:
    charge = -1
if N == 13:
    charge = -1

P_po = symmetry.get_symmetry_permutations(atoms)
U_iXx, P_iXX = symmetry.symmetry_projectors_ptype(P_po)

eps_n = calc.wfs.kpt_u[0].eps_n
f_n = calc.wfs.kpt_u[0].f_n
C_nM = calc.wfs.kpt_u[0].C_nM
q = calc.wfs.kpt_u[0].q
S_MM = calc.wfs.kpt_u[0].S_MM
n_dl = group_eigenvalues(eps_n)

# Loop over degenerate eigenvalue groups
C_diMn = [] # degenerate group, irrep index, actual basis index, symmetry proejcted band index
eps_d = []
f_d = []
for d, n_l in enumerate(n_dl):
    eps_d.append(eps_n[n_l[0]])
    f_d.append(f_n[n_l[0]])
    print('eig: %d x %.5f' % (len(n_l), eps_n[n_l[0]]))
    C_iMn = []
    C_diMn.append(C_iMn)
    for irrep in range(len(U_iXx)):
        U_Mx = U_iXx[irrep]
        #print(U_Mx.T, "U_Mx.t")
        P_MM = np.dot(U_Mx, U_Mx.T)
        #print(P_MM, "P_MM")
        rho_MM = np.dot(C_nM[n_l].T, C_nM[n_l])
        U_MM = np.dot(P_MM.T,  np.dot(S_MM, np.dot(np.dot(rho_MM, S_MM), P_MM)))
        values_n, C_Mn = scipy.linalg.eigh(U_MM, S_MM)
        #print(values_n, C_Mn, "eig")
        C_iMn.append(C_Mn[:, values_n>1e-4])
        first = True
        for n, occ in enumerate(values_n):
            if abs(occ) > 1e-4:
                if first:
                    print(" %.2f  %5s : " % (f_d[d], symmetry.names_i[irrep]), end='')
                    first = False
                print("  %.5f" % (values_n[n]),end='')
        if not first:
            print()

# N is ehspace index
ehspace_N = []
dE_N = []

for occd in range(len(eps_d)):
    occn_l = n_dl[occd]
    if f_d[occd] < 1e-4:
        continue
    for unoccd in range(len(eps_d)):
        if f_d[unoccd] > 1.999:
            continue

        unoccn_l = n_dl[unoccd]
        for occi in range(len(U_iXx)):
            occC_Mn = C_diMn[occd][occi]
            if occC_Mn.shape[1]==0:
                continue
            for unocci in range(len(U_iXx)):
                unoccC_Mn = C_diMn[unoccd][unocci]
                if unoccC_Mn.shape[1]==0:
                    continue
                print('Transition %s %.2f %.5f (%d%s) -> %s %.2f %.5f (%d%s):' % (repr(occn_l), f_d[occd], eps_d[occd], occC_Mn.shape[1], symmetry.names_i[occi], 
                                                                  repr(unoccn_l), f_d[unoccd], eps_d[unoccd], unoccC_Mn.shape[1], symmetry.names_i[unocci]))
                character_ig = np.array(symmetry.character_ig)
                product_g = character_ig[occi, :] * character_ig[unocci, :]
                sizes_g = np.array(list(map(len, symmetry.ops_g)))
                print("%3s x %3s = " % (symmetry.names_i[occi], symmetry.names_i[unocci]), end='')
                for target_irrep in range(len(U_iXx)):
                    proj = np.sum(product_g * sizes_g * character_ig[target_irrep, :].T) / 48
                    if np.abs(proj)>1e-4:
                        if target_irrep in target_irreps:
                            print(" {",end='')
                        print(" %3s (%.5f) " % (symmetry.names_i[target_irrep], proj), end='')
                        if target_irrep in target_irreps:
                            print("} ",end='')

                print()
                for target_irrep in target_irreps:
                    redocc_onn = []
                    redunocc_onn = []
                    rho_nnnn = 0.0
                    sizes_g = np.array(list(map(len, symmetry.ops_g)))
                    
                    
                    for o in range(48):
                        P_MM = symmetry.symmetry_operator_ptype(P_po, o)
                        redocc_nn = np.dot(occC_Mn.T, np.dot(S_MM, np.dot(P_MM, occC_Mn)))
                        redocc_onn.append(redocc_nn)
                        redunocc_nn = np.dot(unoccC_Mn.T, np.dot(S_MM, np.dot(P_MM, unoccC_Mn)))
                        redunocc_onn.append(redunocc_nn)
                        chi = symmetry.character_ig[target_irrep, symmetry.g_o[o]]
                        rho_nnnn += np.kron(redocc_nn, redunocc_nn) * chi / 48 * symmetry.character_ig[target_irrep, -1] # XXX Hard coded -1
                    #print(rho_nnnn, "rho_nnnn!!!")
                    assert(np.linalg.norm(rho_nnnn-rho_nnnn.T)<1e-8)
                    vals, vectors_nnk = np.linalg.eigh(rho_nnnn)
                    #print(vectors_nnk, "VEC!!!")
                    tot = np.trace(rho_nnnn)
                    if abs(tot)>1e-4:
                        print(" %s (%.5f) " % (symmetry.names_i[target_irrep], tot), end='')
                        print(" eigs: ", end='')
                        for k, val in enumerate(vals):
                            if abs(val)>1e-4:
                                print("%05d=%.3f " % (len(ehspace_N), val), end ='')
                                ehspace_N.append( (occn_l, unoccn_l, eps_d[occd], eps_d[unoccd], f_d[occd], f_d[unoccd], occC_Mn, unoccC_Mn, vectors_nnk[:,k]) )
                                dE_N.append(eps_d[unoccd]-eps_d[occd])

                                print()
                                
def charge_density(occC_Mn, unoccC_Mn, vec_nn, do_pack2=False):
    rho_MM = np.dot(occC_Mn, np.dot(np.reshape(vec_nn, (occC_Mn.shape[1], unoccC_Mn.shape[1]) ), unoccC_Mn.T))
    
    #print("Norm of symmetry adapted rho_MM (should be 0)", np.trace(np.dot(S_MM, rho_MM)))
    rho_MM = (rho_MM + rho_MM.T)/2
    rho_MM = rho_MM.real.copy()
    Q_aL = {}
    D_ap = {}
    for a in range(len(atoms)):
        P_Mi = calc.wfs.P_aqMi[a][q]
        D_ii = np.dot(P_Mi.T, np.dot(rho_MM, P_Mi))
        if do_pack2:
            D_p = pack2(D_ii)
        else:
            D_p = pack(D_ii)
        D_ap[a] = D_p
        Q_aL[a] = np.dot(D_p, calc.density.setups[a].Delta_pL).real.copy()
    rho_G = calc.density.gd.zeros()
    calc.wfs.basis_functions.construct_density(rho_MM, rho_G, q)
    rho_g = calc.density.finegd.zeros()
    calc.density.interpolator.apply(rho_G, rho_g)
    calc.density.ghat.add(rho_g, Q_aL)
    return rho_g, D_ap

N_N = np.argsort(dE_N)
print(N_N)
ordehspace_N = []
for N in N_N:
    ordehspace_N.append(ehspace_N[N])

f = open(argv[1]+'.symtddft_new','w')

for N1, (occ1_l, unocc1_l, occeps1, unocceps1, occf1, unoccf1, occC1_Mn, unoccC1_Mn, vec1_nn) in enumerate(ordehspace_N):
    rho1_g, D1_ap = charge_density(occC1_Mn, unoccC1_Mn, vec1_nn)
    VHt1_g = calc.density.finegd.zeros()
    #print(calc.hamiltonian.poisson.remove_moment)
    calc.hamiltonian.poisson.solve(VHt1_g, rho1_g, charge=None)

    H_ap = {}
    for a, D1_p in D1_ap.items():
        C_pp = calc.wfs.setups[a].M_pp
        H_ap[a] = 2.0 * np.dot(C_pp, D1_p)
    
    for N2, (occ2_l, unocc2_l, occeps2, unocceps2, occf2, unoccf2, occC2_Mn, unoccC2_Mn, vec2_nn) in enumerate(ordehspace_N[:N1+1]):
        rho2_g, D2_ap = charge_density(occC2_Mn, unoccC2_Mn, vec2_nn)
        K = calc.density.finegd.integrate(VHt1_g * rho2_g)
        for a, D2_p in D2_ap.items():
            #print("Corr", np.dot(D2_p, H_ap[a]))
            K = K + np.dot(D2_p, H_ap[a])

        if N1 == N2:
            mu = calc.density.finegd.calculate_dipole_moment(rho1_g, center=True)
        else:
            mu = [0.0,0.0,0.0]
        print(" [%s -> %s] V [%s -> %s ]" % (repr(occ1_l), repr(unocc1_l), repr(occ2_l), repr(unocc2_l)))
        print("%05d %05d %.15f %.15f %.15f %.15f %.15f %.15f" % (N1, N2, K, unocceps1-occeps1, unocceps2-occeps2, mu[0], mu[1], mu[2]))
        print("%05d %05d %.15f %.15f %.15f %.15f %.15f %.15f" % (N1, N2, K, unocceps1-occeps1, unocceps2-occeps2, mu[0], mu[1], mu[2]), file=f)
    f.flush()
f.close()

